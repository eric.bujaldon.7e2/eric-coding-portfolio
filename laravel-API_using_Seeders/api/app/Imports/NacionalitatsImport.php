<?php

namespace App\Imports;

use App\Models\Nacionalitat;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class NacionalitatsImport implements ToCollection, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row) {
            if ($row[0] != 'Any') {
                $ok = Nacionalitat::create([
                    'any' => $row[0],
                    'codi_districte' => $row[1],
                    'nom_districte' => $row[2],
                    'codi_barri' => $row[3],
                    'nom_barri' => $row[4],
                    'sexe' => $row[5],
                    'nacionalitat' => $row[6],
                    'nombre' => $row[7],
                ]);
            }
        }
    }
    public function chunkSize(): int
    {
        return 1000;
    }
}
