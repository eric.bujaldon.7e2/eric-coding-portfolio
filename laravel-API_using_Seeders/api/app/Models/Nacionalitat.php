<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nacionalitat extends Model
{
    use HasFactory;
    protected $fillable = ['any', 'codi_districte', 'nom_districte', 'codi_barri', 'nom_barri', 'sexe', 'nacionalitat', 'nombre'];
    public $timestamps = false;
}
