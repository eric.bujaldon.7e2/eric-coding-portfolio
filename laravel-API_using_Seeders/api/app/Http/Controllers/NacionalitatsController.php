<?php

namespace App\Http\Controllers;

use App\Models\Nacionalitat;
use Illuminate\Http\Request;

class NacionalitatsController extends Controller
{
    
    public function totesNacionalitats(Request $request)
    {
        $nacionalitat = Nacionalitat::all();
        return $nacionalitat;
    }

    public function buscarNacionalitat(Request $request)
    {
        $nacionalitat = Nacionalitat::query()
            ->where('_id', $request->id)
            ->get();
        return $nacionalitat;
    }

    public function nomDistricte(Request $request)
    {
        $nacionalitat = Nacionalitat::query()
            ->where('nom_districte', $request->nom_districte)
            ->get();
        return $nacionalitat;
    }

    public function nomBarri(Request $request)
    {
        $nacionalitat = Nacionalitat::query()
            ->where('nom_barri', $request->nom_barri)
            ->get();
        return $nacionalitat;
    }

    public function nacionalitat(Request $request)
    {
        $nacionalitat = Nacionalitat::query()
            ->where('Nacionalitat', $request->nacionalitat)
            ->get();
        return $nacionalitat;
    }

    public function sexe(Request $request)
    {
        $nacionalitat = Nacionalitat::query()
            ->where('Sexe', $request->sexe)
            ->get();
        return $nacionalitat;
    }




}
