<?php

namespace App\Http\Controllers;

use App\Imports\NacionalitatsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function nacionalitatImport() 
    {
        Excel::import(new NacionalitatsImport, '2021_ine_nacionalitat_per_sexe.csv');
    }
}
