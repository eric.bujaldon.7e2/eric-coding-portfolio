<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/nacionalitats', 'App\Http\Controllers\NacionalitatsController@totesNacionalitats');
Route::get('/nacionalitats/{id}', 'App\Http\Controllers\NacionalitatsController@buscarNacionalitat');

Route::get('/nacionalitats/districte/{nom_districte}', 'App\Http\Controllers\NacionalitatsController@nomDistricte');
Route::get('/nacionalitats/barri/{nom_barri}', 'App\Http\Controllers\NacionalitatsController@nomBarri');

Route::get('/nacionalitats/nacionalitat/{nacionalitat}', 'App\Http\Controllers\NacionalitatsController@nacionalitat');
Route::get('/nacionalitats/sexe/{sexe}', 'App\Http\Controllers\NacionalitatsController@sexe');
