<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    public function run()
    {
        for ($numUsers=0; $numUsers<30; $numUsers++) {
            DB::table('users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@itb.cat',
                'password' => Hash::make('password'),
            ]);
        }
    }
}
