<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNacionalitatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nacionalitats', function (Blueprint $table) {
            $table->increments('_id');
            $table->integer('any');
            $table->integer('codi_districte');
            $table->string('nom_districte', 100);
            $table->integer('codi_barri');
            $table->string('nom_barri', 100);
            $table->string('sexe', 100);
            $table->string('nacionalitat', 100);
            $table->integer('nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nacionalitats');
    }
}
