# This is an Laravel API Web Application made by Eric, using Seeders

This project focuses on the use and creation of an API through web services. I've chosen a set of data (dataset) in CSV format from the Barcelona Opendata page. 
The dataset I have chosen to perform this project is
Official population by nationality and sex of the city of Barcelona.
This dataset stores the number of people of each sex in different neighborhoods and districts. The data is from the year 2021. It is in CSV format.

The API Queries I made for this app are:

## totesNacionalitats: Show all data from the database.
### example: 127.0.0.1:8000/api/nacionalitats
![Image of the app](totesNacionalitats.PNG "Laravel API")

## buscarNacionalitat: Show only data from the inserted id.
### example: 127.0.0.1:8000/api/nacionalitats/{id}
![Image of the app](buscarNacionalitat.PNG "Laravel API")

## nomDistricte: Show all data from the inserted district.
### example: 127.0.0.1:8000/api/nacionalitats/districte/{nomDistricte}
![Image of the app](nomDistricte.PNG "Laravel API")

## nomBarri: Show all data from the inserted neighborhood.
### example: 127.0.0.1:8000/api/nacionalitats/barri/{nomBarri}
![Image of the app](nomBarri.PNG "Laravel API")

## nacionalitat: Show all data from the inserted nationality.
### example: 127.0.0.1:8000/api/nacionalitats/nacionalitat/{nacionalitat}
![Image of the app](nacionalitat.PNG "Laravel API")

## sexe: Show all data from the inserted gender.
### example: 127.0.0.1:8000/api/nacionalitats/sexe/{sexe}
![Image of the app](sexe.PNG "Laravel API")

# How to run the app

### Starting Laravel development server command:

php artisan serve
