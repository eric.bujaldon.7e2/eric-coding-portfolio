# This is my final project of Web Development Grade, made using PHP and Bootstrap

It is a food recipe social network, where you can upload your own recipes with your own profile and see other people recipes.

## To check the Web App online, go to the following website:

https://ericbujaldon7e2.alwaysdata.net/kitchees/

### To run the app, put the app in var/www/html, open a browser and write on the url:

localhost/Proyecto_Final_Grado_Kitchees/

## Important!!

The database account where I had all the images in, got deleted. So you will see an "example" image in every receipt of teh webpage.

If you want to check out the user-only features of the page, please Sign in as one of the following users:

- Username: EricBM - Password: eric123

- Username: serinito1 - Password: 111

The user-only features are: Save favorite recipe, generate random recipe, profile page and create recipe.

If you are not logged, you can only see recipes created from other users.

### Thank you!



