<?php
class Receta{
    static $contador = 1;
    public $id;
    public $nombre;
    public $categoria;
    public $pais;
    public $descripcion;
    public $ingredientes;
    public $elaboracion;
    public $foto1;
    public $foto2;
    public $dificultad;

    function __construct(){
        $this->id = $this::$contador;
        $this::$contador++;
    }

    function getId(){
        return $this->id;
    }

    function toArray(){
        $tmpArray = array(
            "nombre"=>$this->nombre,
            "categoria"=>$this->categoria,
            "pais"=>$this->pais,
            "descripcion"=>$this->descripcion,
            "ingredientes"=>$this->ingredientes,
            "elaboracion"=>$this->elaboracion,
            "foto1"=>$this->foto1,
            "foto2"=>$this->foto2,
            "dificultad"=>$this->dificultad
        );
        return $tmpArray;
    }
}

class Pais{
    static $contador = 1;
    public $id;
    public $foto;
    public $nombre;
    public $descripcion;


    function __construct(){
        $this->id = $this::$contador;
        $this::$contador++;
    }

    function getId(){
        return $this->id;
    }

    function addFoto($foto){
        $this->fotos[] = $foto;
    }

    function toArray(){
        $tmpArray = array(
            "foto"=>$this->$foto,
            "nombre"=>$this->$nombre,
            "descripcion"=>$this->descripcion,
            "categoria"=>$this->categoria,
        );
        return $tmpArray;
    }
}

class Recetas{
    public $kRecetas = [];

    function addReceta($receta){
        $this->kRecetas[] = $receta;
    }

    function getRecetaById($id){
        foreach($this->kRecetas as $receta){
            if ($receta->id == $id){
                return $receta;
            }
        }
        return "No existe esta receta";
    }

    function listar(){
        foreach($this->kRecetas as $receta){
            print_r($receta);
        }
    }
}

class Paises{
    public $kPaises = [];

    function addPais($pais){
        $this->kPaises[] = $pais;
    }

    function getPaisById($id){
        foreach($this->kPais as $pais){
            if ($pais->id == $id){
                return $pais;
            }
        }
        return "No existe este pais";
    }

    function listar(){
        foreach($this->kPais as $pais){
            print_r($pais);
        }
    }
}
?>