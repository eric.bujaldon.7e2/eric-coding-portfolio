<html>

<head>
<meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitchees</title>


    <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">

    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="../assets/css/receta.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
</head>

<body>

    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="../index.html"><img src="../assets/img/kitcheeslogo.png" width="300" height="80"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="../index.html#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#about">Sobre nosotros</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#services">Servicios</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#contact">Contact</a></li>
                    <li><a class="nav-link scrollto" href="../paginas/paises.php">Recetas</a></li>
                    <?php include('perfil.php');?>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

        </div>
    </header>
    <section class="about">
      <div class="container">
        <div class="row">
          <?php
            $page = 2;
            //session_start();
            include('DB_connexio.php');
            $id = $_GET['id'];
                $result = $conn->query("SELECT * FROM recetas WHERE ID='$id'");
                  foreach($result as $row) {

                    echo "<div class='pt-5 text-center' >";
                        echo '<img class=\'border border-3 border-dark rounded-pill w-75\'  src=\'uploads/recetas/' . $row['foto2'].'\' width=\'100%\'>';                             
                        echo "<h3 class='mt-5 p'>". $row['nombre']."</h3>";
                        echo "<div class='row'>";
                        echo "<table class='table table-hover table-borderless d-flex justify-content-center'>";
                        echo "<tbody>";
                        echo "<tr>";
                        echo "<td class='in'>Categoria</td>";
                        echo "<td>". $row['categoria']."</td>";     
                            echo "</tr>";
                            echo "<tr> ";
                                echo "<td class='in'>Dificultad</td>";
                                echo "<td>". $row['dificultad']."</td>";
                                echo "</tr>";
                                echo "</tbody>";
                                echo "</table>";
                        echo "<p class='mt-5'>INGREDIENTES</p>";
                        echo "<div class='d-flex justify-content-center ingr'>";
                        echo "<table class='table table-borderless d-flex justify-content-center'>";
                            echo "<tbody>";
                                echo "<tr> ";                            
                                    echo "<td>". $row['ingredientes']."</td>";
                                echo "</tr>";
                            echo "</tbody>";
                        echo "</table>";
                        echo "</div>";
                        echo "<p class='mt-5'>ELABORACIÓN</p>";
                        echo"<h6>". $row['elaboracion']."</h6>";
                        echo "<div class='mt-4 p-4'>";
                            if(isset($_SESSION['session_nombreUsuario'])){
                                $currentUser = $_SESSION['session_nombreUsuario'];
                                $query = $conn->query("SELECT * FROM usuarios WHERE username = '$currentUser'");
                                foreach($query as $row) {
                                    $idUser = $row['ID'];
                                }
                                $result8 = $conn->query("SELECT * FROM favorites WHERE userID = $idUser AND productID = $id");
                                if ($result8 -> num_rows >0){
                                    echo '<button type=\'button\' class=\'btn btn-secondary \'><a id =\'cierreSE\'  href=\'delFavorito.php?idfav=' . $id . '\'>Eliminar de tus recetas favoritas</a></button>';                             
                                }
                                else {
                                    echo '<button type=\'button\' class=\'btn btn-secondary \'><a id =\'cierreSE\'  href=\'addFavorito.php?idfav=' . $id . '\'>Añadir a tus recetas favoritas</a></button>';                             
                                    
                                }
                            }
                        echo "</div>";
                        echo "</div>";
                        
                    echo "</div>";  
                }
            
            
            ?>
        </div>
      </div>
    </section>
    
<footer id="footer">
<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 footer-contact">
                <img src="../assets/img/kitcheeslogo.png" width="220" height="70">
                <p>
                    Mall de Dubai <br>
                    Financial Center Street<br>
                    Dubai - Emirates Árabes <br><br>
                    <strong>Teléfono:</strong> +1 5589 55488 55<br>
                    <strong>Email:</strong> kitcheesBCN@gmail.com<br>
                </p>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Links útiles</h4>
                <ul>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#hero">Home</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#about">Sobre nosotros</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Servicios</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#faq">Preguntas frecuentes</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#contact">Contacto</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Nuestros Servicios</h4>
                <ul>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Acceder a recetas de todo el
                            mundo</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poder crear tus propias
                            recetas</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poseer una lista de recetas
                            favoritas</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Generación de receta aleatoria y
                            mucho más</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Nuestras Redes Sociales</h4>
                <p>Síguenos en todas nuestras redes! <br>Te esperamos!</p>
                <div class="social-links mt-3">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                </div>
            </div>

        </div>
    </div>
</div>
</footer>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
    class="bi bi-arrow-up-short"></i></a>

<script src="../assets/vendor/aos/aos.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="../assets/vendor/php-email-form/validate.js"></script>

<script src="../assets/js/main.js"></script>
</body>


</html>
