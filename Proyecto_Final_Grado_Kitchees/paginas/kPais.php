<?php
include("kModel.php");

$pais1 = new Pais();
$pais1->foto = "../assets/img/dm.jpg";
$pais1->nombre = "Dinamarca";
$pais1->descripcion = "Las características generales asociadas a sus platos, es que consisten principalmente en pescados, mariscos, carne y una variedad de raíces comestibles, verduras y hierbas.";


$pais2 = new Pais();
$pais2->foto = "../assets/img/italia.jpg";
$pais2->nombre = "Italia";
$pais2->descripcion = "La gastronomía de Italia se caracteriza por sus elaboraciones con abundantes verduras, frutas, carnes, pescados, arroz, pastas y panes.";


$pais3 = new Pais();
$pais3->foto = "../assets/img/peru.jpg";
$pais3->nombre = "Peru";
$pais3->descripcion = "Los platos típicos de Perú son fruto del mestizaje y la influencia de África, España, Italia, China y Japón.";


$pais4 = new Pais();
$pais4->foto = "../assets/img/grecia.jpg";
$pais4->nombre = "Grecia";
$pais4->descripcion = "LAl igual que en el resto de la cuenca mediterránea, el ingrediente más antiguo y característico de la cocina griega es el aceite de oliva, que está presente en casi todos los platos.";


$pais5 = new Pais();
$pais5->foto = "../assets/img/fr.jpg";
$pais5->nombre = "Francia";
$pais5->descripcion = "La gastronomía francesa ha sido reconocida por la UNESCO como Patrimonio Cultural Inmaterial de la Humanidad.";


$pais6 = new Pais();
$pais6->foto = "../assets/img/mrr.jpg";
$pais6->nombre = "Marruecos";
$pais6->descripcion = "La culinaria marroquí se puede considerar como de gran riqueza y diversidad, esto se puede deber a la interacción que ha llegado a tener con otras culturas externas.";


$pais7 = new Pais();
$pais7->foto = "../assets/img/br.jpg";
$pais7->nombre = "Brasil";
$pais7->descripcion = "La gastronomía de Brasil tiene influencia de la cocina europea, africana e indígena. El plato nacional de la gastronomía brasileña es la feijoada.";


$pais8 = new Pais();
$pais8->foto = "../assets/img/uru.jpg";
$pais8->nombre = "Uruguay";
$pais8->descripcion = "La gastronomía uruguaya es una mezcla entre la cocina española, la italiana, y otros países de Europa, aunque con menor influencia, además de la cultura amerindia.";


$pais9 = new Pais();
$pais9->foto = "../assets/img/esp.jpg";
$pais9->nombre = "España";
$pais9->descripcion = "Cocina de origen que oscila entre el estilo rural y el costero, representa una diversidad fruto de muchas culturas, así como de paisajes y climas.¡VIVA!";


$pais10 = new Pais();
$pais10->foto = "../assets/img/mx.jpg";
$pais10->nombre = "Mejico";
$pais10->descripcion = "La comida mexicana cuenta con una gran diversidad de platos típicos, donde sus principales ingredientes son el maíz, el cilantro, el chile, el frijol, el piloncillo, el nopal y el jitomate.";

$misPaises = new Paises();
$misPaises->addPais($pais1);
$misPaises->addPais($pais2);
$misPaises->addPais($pais3);
$misPaises->addPais($pais4);
$misPaises->addPais($pais5);
$misPaises->addPais($pais6);
$misPaises->addPais($pais7);
$misPaises->addPais($pais8);
$misPaises->addPais($pais9);
$misPaises->addPais($pais10);



?>