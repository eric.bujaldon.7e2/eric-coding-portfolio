<?php
session_start();
if(isset($_SESSION['session_nombreUsuario'])){
}else {
    header('location: login.php');
    
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitchees</title>


    <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">

    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="../index.html"><img src="../assets/img/kitcheeslogo.png" width="300" height="80"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="../index.html#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#about">Sobre nosotros</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#services">Servicios</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#contact">Contact</a></li>
                    <li><a class="nav-link scrollto" href="../paginas/paises.php">Recetas</a></li>
                    <?php include('perfil.php');?>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

        </div>
    </header>

    <section class="services section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <p style="margin-top: 50px">¡Bienvenido <?php  if(isset($_SESSION['session_nombreUsuario'])){echo $_SESSION['session_nombreUsuario'];}?>!</p>
            </div>
            
            <div>
                <?php
                    include("DB_connexio.php");
                    if(isset($_SESSION['session_nombreUsuario'])){
                        $user = $_SESSION['session_nombreUsuario'];
                        $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
                        foreach($query as $row) {
                            $imageURL3 = 'uploads/usuarios/'.$row["pfp"];
                            $id = $row['ID'];
                        }
                        echo "<div class='d-flex justify-content-center'>";
                        echo "<img class='logMi mb-5' src='$imageURL3'>";
                        echo "</div>";
                    }
                ?> 
            <div>
                
            
        
            <div>
                <h2>Tus datos</h2>
                <div class='datos'>
                    <table class='table table-hover border-dark d-flex justify-content-center'>
                        <tbody>
                            <tr>
                                <td class="in">CORREO</td>
                                <td><?php
                                 $user = $_SESSION['session_nombreUsuario'];
                                 $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
                                 foreach($query as $row) {
                                     $correo = $row["email"];
                                 }
                                 echo $correo;
                                ?> </td>
                                
                            </tr>
                            <tr>                             
                                <td class="in">USUARIO</td>
                                <td><?php
                                 $user = $_SESSION['session_nombreUsuario'];
                                 $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
                                 foreach($query as $row) {
                                     $correo = $row["username"];
                                 }
                                 echo $correo;
                                ?> </td>
                                
                            </tr>
                            <tr>
                                <td class="in">NOMBRE</td>
                                <td><?php
                                 $user = $_SESSION['session_nombreUsuario'];
                                 $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
                                 foreach($query as $row) {
                                     $correo = $row["name"];
                                 }
                                 echo $correo;
                                ?> </td>
                            </tr>
                        </tbody>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-9 mt-5">
            <h4>Quieres que todo el mundo pueda disfrutar de tus platos?</h4>
            <p>Desde Kitchees te animamos a que subas tus mejores recetas a nuestra web, para que todo el mundo pueda deleitarse con ellas!</p>
          </div>
          <div class="d-flex justify-content-end">
            <button type='button' class='btn color'><a id='cierreSE' href='../paginas/crearReceta.php'>Crear receta</a></button>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-9 mt-5">
            <h4>Tus recetas favoritas</h4>
            <p>Guarda tu recetas para no olvidarlas nunca, en este apartado las podras encontrar, de esta manera las podras hacer cuando quieras!</p>
          </div>
          <div class="d-flex justify-content-end">
            <button type='button' class='btn color'><?php
                    include("DB_connexio.php");
                    if(isset($_SESSION['session_nombreUsuario'])){
                        $user = $_SESSION['session_nombreUsuario'];
                        $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
                        foreach($query as $row) {
                            
                            $id = $row['ID'];
                        }
                        
                        echo '<a id =\'cierreSE\' href=\'showFavorites.php?idUser=' . $id . '\'>Ver recetas</a>';
                    }
                ?></button>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-9 mt-5">
            <h4>¿No sabes que cocinar?</h4>
            <p>Pues no te preocupes, Kitchees te recomendara de forma aleatoria una receta con la que te chuparás los dedos!</p>
          </div>
          <div class="d-flex justify-content-end">
            <button type='button' class='btn color'><a id ="cierreSE" href="generarReceta.php">Receta aleatoria</a></button>
          </div>
        </div>
      </div>
    </section>
    
      
   

    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <img src="../assets/img/kitcheeslogo.png" width="220" height="70">
                        <p>
                            Mall de Dubai <br>
                            Financial Center Street<br>
                            Dubai - Emirates Árabes <br><br>
                            <strong>Teléfono:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> kitcheesBCN@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Links útiles</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#hero">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#about">Sobre nosotros</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Servicios</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#faq">Preguntas frecuentes</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#contact">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestros Servicios</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Acceder a recetas de todo el
                                    mundo</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poder crear tus propias
                                    recetas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poseer una lista de recetas
                                    favoritas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Generación de receta aleatoria y
                                    mucho más</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestras Redes Sociales</h4>
                        <p>Síguenos en todas nuestras redes! <br>Te esperamos!</p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <script src="../assets/vendor/aos/aos.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="../assets/vendor/php-email-form/validate.js"></script>

    <script src="../assets/js/main.js"></script>
</body>

</html>