<?php
session_start();
if(isset($_SESSION['session_nombreUsuario'])){
}else {
    header('location: login.php');
    
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitchees</title>


    <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">

    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="../index.html"><img src="../assets/img/kitcheeslogo.png" width="300" height="80"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="../index.html#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#about">Sobre nosotros</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#services">Servicios</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#contact">Contact</a></li>
                    <li><a class="nav-link scrollto" href="../paginas/paises.php">Recetas</a></li>
                    <?php include('perfil.php');?>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

        </div>
    </header>
    </section>

    <section class="services section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <p style="margin-top: 50px">Crea tu receta</p>
            </div>

            <div class="row">
                <div class="align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box3">
                        <form id="formulario" enctype="multipart/form-data" action="../paginas/DB_crearReceta.php"
                            method="POST">
                            <div class="row mb-4">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-outline mb-4">
                                        <input type="text" id="name" name="name" class="form-control" />
                                        <label class="form-label" for="name">Nombre de la receta</label><br>
                                        <small id="errorName" class="text-danger"></small>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-outline mb-4">
                                        <select type="text" id="category" name="category" class="form-select">
                                            <option value="Dulces">Dulces</option>
                                            <option value="Carnes">Carnes</option>
                                            <option value="Pasta">Pasta</option>
                                            <option value="Entrantes">Entrantes</option>
                                            <option value="Pescados">Pescados</option>
                                            <option value="Sopas">Sopas</option>
                                            <option value="Ensaladas">Ensaladas</option>
                                            <option value="Postres">Postres</option>
                                            <option value="Arroces">Arroces</option>
                                        </select>
                                        <label class="form-label" for="category">Categoria</label><br>
                                        <small id="errorCategory" class="text-danger"></small>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-4">
                                    <div class="form-outline mb-4">
                                        <select type="text" id="country" name="country" class="form-select">
                                            <option value="null" selected>Selecciona un país</option>
                                            <option value="Dinamarca">Dinamarca</option>
                                            <option value="Italia">Italia</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Grecia">Grecia</option>
                                            <option value="Francia">Francia</option>
                                            <option value="Marruecos">Marruecos</option>
                                            <option value="Brasil">Brasil</option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="España">España</option>
                                            <option value="Mejico">Mejico</option>
                                        </select>
                                        <label class="form-label" for="country">País</label><br>
                                        <small id="errorCountry" class="text-danger"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-outline mb-4">
                                <textarea type="text" id="description" name="description" class="form-control"
                                    placeholder="Introduce una pequeña descripción de la receta." rows="2"></textarea>
                                <label class="form-label" for="description">Descripción</label><br>
                                <small id="errorDescription" class="text-danger"></small>
                            </div>

                            <div class="form-outline mb-4">
                                <textarea type="text" id="ingredients" name="ingredients" class="form-control"
                                    placeholder='Introduce los ingredientes con este formato: "ingrediente cantidad / ingrediente cantidad ..."'
                                    rows="4"></textarea>
                                <label class="form-label" for="ingredients">Ingredientes</label><br>
                                <small id="errorIngredients" class="text-danger"></small>
                            </div>

                            <div class="form-outline mb-4">
                                <textarea type="text" id="elaboration" name="elaboration" class="form-control"
                                    placeholder='Introduce la forma de elaborar tu recta con este formato: "Paso 1: ..."'
                                    rows="5"></textarea>
                                <label class="form-label" for="elaboration">Elaboración</label><br>
                                <small id="errorElaboration" class="text-danger"></small>
                            </div>

                            <div class="form-outline mb-4">
                                <select type="text" id="dificulty" name="dificulty" class="form-select">
                                    <option value="null" selected>Selecciona la dificultad de la receta</option>
                                    <option value="Facil">Facil</option>
                                    <option value="Media">Media</option>
                                    <option value="Dificil">Dificil</option>
                                </select>
                                <label class="form-label" for="dificulty">Dificultad</label><br>
                                <small id="errorDificulty" class="text-danger"></small>
                            </div>

                            <div class="row mb-4">
                                <div class="col-sm-12 col-md-6 col-lg-6 ">
                                    <input id="uploadFile" placeholder="Foto de portada" disabled="disabled" />

                                    <div class="btn btn-primary fileUpload">
                                        <input id="image1" type="file" class="upload" name="image1" />
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                                            fill="currentColor" class="bi bi-upload" viewBox="0 0 16 16">
                                            <path
                                                d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                                            <path
                                                d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
                                        </svg>
                                    </div>
                                    <small id="errorImage1" class="text-danger"></small>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 ">
                                    <input id="uploadFile2" placeholder="Foto de detalle" disabled="disabled" />

                                    <div class="btn btn-primary fileUpload">
                                        <input id="image2" type="file" class="upload" name="image2" />
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                                            fill="currentColor" class="bi bi-upload" viewBox="0 0 16 16">
                                            <path
                                                d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                                            <path
                                                d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
                                        </svg>
                                    </div>
                                    <small id="errorImage2" class="text-danger"></small>
                                </div>
                                <label class="form-label" for="dificulty">Fotos de la receta</label><br>
                            </div>
                            <div class="d-flex justify-content-center">
                                <button id="btn-register" type="submit" class="btn btn-secondary btn-block mb-4">Crear
                                    receta</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <img src="../assets/img/kitcheeslogo.png" width="220" height="70">
                        <p>
                            Mall de Dubai <br>
                            Financial Center Street<br>
                            Dubai - Emirates Árabes <br><br>
                            <strong>Teléfono:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> kitcheesBCN@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Links útiles</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#hero">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#about">Sobre nosotros</a>
                            </li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Servicios</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#faq">Preguntas frecuentes</a>
                            </li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#contact">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestros Servicios</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Acceder a recetas
                                    de todo el
                                    mundo</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poder crear tus
                                    propias
                                    recetas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poseer una lista de
                                    recetas
                                    favoritas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Generación de
                                    receta aleatoria y
                                    mucho más</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestras Redes Sociales</h4>
                        <p>Síguenos en todas nuestras redes! <br>Te esperamos!</p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <script src="../assets/vendor/aos/aos.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="../assets/vendor/php-email-form/validate.js"></script>

    <script src="../assets/js/main.js"></script>
</body>
<script>
    document.getElementById("image1").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
    };
    document.getElementById("image2").onchange = function () {
        document.getElementById("uploadFile2").value = this.value;
    };
    document.getElementById("btn-register").addEventListener("click", comprovarFormulari)

    function comprovarFormulari(event) {
        event.preventDefault();

        var arrayMissatges = document.getElementsByTagName("small");
        for (var validacio of arrayMissatges) {
            validacio.innerHTML = "";
        };

        var inputValidacio = document.getElementsByTagName("input");
        for (var borde of inputValidacio) {
            borde.style.borderColor = "gray";
        };

        var textAreaValidacio = document.getElementsByTagName("textarea");
        for (var textarea of textAreaValidacio) {
            textarea.style.borderColor = "gray";
        };

        var selectValidacio = document.getElementsByTagName("select");
        for (var select of selectValidacio) {
            select.style.borderColor = "gray";
        };

        if ((validacioNombre() && validacioCategoria() && validacioPais() && validacioDescripcion() && validacioIngredientes() && validacioElaboracion() && validacioDificultad() && validacioFoto1() && validacioFoto2()) == true) {
            document.getElementById("formulario").submit();
        }
    }

    function validacioNombre() {
        var name = document.getElementById("name");

        if (name.value.length <= 1) {
            document.getElementById("errorName").innerHTML = "¡El nombre no puede estar vacio!"
            document.getElementById("name").style.borderColor = "red";
            return false;
        }
        return true;

    }

    function validacioCategoria() {
        var category = document.getElementById("category");

        if (category.value.length <= 0) {
            document.getElementById("errorCategory").innerHTML = "¡Escribe un nombre!"
            document.getElementById("category").style.borderColor = "red";
            return false;
        }
        return true;
    }


    function validacioPais() {
        var country = document.getElementById("country");

        if (country.value == "null") {
            document.getElementById("errorCountry").innerHTML = "¡Selecciona un país!"
            document.getElementById("country").style.borderColor = "red";
            return false;
        }
        return true;
    }


    function validacioDescripcion() {
        var description = document.getElementById("description");

        if (description.value.length <= 10) {
            document.getElementById("errorDescription").innerHTML = "¡Escribe la descripción de tu receta!"
            document.getElementById("description").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioIngredientes() {
        var ingredients1 = document.getElementById("ingredients");

        if (ingredients1.value.length <= 0) {
            document.getElementById("errorIngredients").innerHTML = "¡Escribe los ingredientes necesarios para realizar esta receta!"
            document.getElementById("ingredients").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioElaboracion() {
        var elabo = document.getElementById("elaboration");

        if (elabo.value.length <= 0) {
            document.getElementById("errorElaboration").innerHTML = "¡Escribe los pasos necesarios para realizar esta receta!"
            document.getElementById("elaboration").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioDificultad() {
        var dificulty = document.getElementById("dificulty");

        if (dificulty.value == "null") {
            document.getElementById("errorDificulty").innerHTML = "¡Especifica la dificultad de la receta!"
            document.getElementById("dificulty").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioFoto1() {
        var image1 = document.getElementById("image1");

        if (image1.value.length <= 0) {
            document.getElementById("errorImage1").innerHTML = "Selecciona una imagen!"
            document.getElementById("image1").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioFoto2() {
        var image2 = document.getElementById("image2");

        if (image2.value.length <= 0) {
            document.getElementById("errorImage2").innerHTML = "Selecciona una imagen!"
            document.getElementById("image2").style.borderColor = "red";
            return false;
        }
        return true;
    }

</script>

</html>