<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitchees</title>


    <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">

    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>

    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="../index.html"><img src="../assets/img/kitcheeslogo.png" width="300" height="80"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="../index.html#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#about">Sobre nosotros</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#services">Servicios</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#contact">Contact</a></li>
                    <li><a class="nav-link scrollto" href="../paginas/paises.php">Paises</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

        </div>
    </header>

    <section class="services section-bg">
        <div class="container" data-aos="fade-up">
  
          <div class="section-title">
            <p style="margin-top: 50px">Registro de Usuario</p>
          </div>
  
          <div class="row">
            <div class="align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box2">
                <form id="formulario" enctype="multipart/form-data" action="../paginas/DB_crearUsuari.php" method="POST">
                    <div class="row mb-4">
                      <div class="col">
                        <div class="form-outline">
                          <input type="text" id="nombreUsuario" name="nombreUsuario" class="form-control" />
                          <label class="form-label" for="nombreUsuario">Nombre de usuario</label><br>
                          <small id="errorUsuario" class="text-danger"></small>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-outline">
                          <input type="text" id="nombre" name="nombre" class="form-control" />
                          <label class="form-label" for="nombre">Nombre completo</label><br>
                          <small id="errorNombre" class="text-danger"></small>
                        </div>
                      </div>
                    </div>

                    <div class="form-outline mb-4">
                      <input type="email" id="email" name="email" class="form-control" />
                      <label class="form-label" for="email">Dirección de correo</label><br>
                      <small id="errorEmail" class="text-danger"></small>
                    </div>

                    <div class="form-outline mb-4">
                      <input type="password" id="contra" name="contra" class="form-control" />
                      <label class="form-label" for="pass">Contraseña</label><br>
                      <small id="errorPassword" class="text-danger"></small>
                    </div>
                    <div class="form-outline mb-4">
                        <input type="file" id="foto" name="foto" class="form-control" />
                        <label class="form-label" for="pfp">Foto de perfil</label><br>
                        <small id="errorFoto" class="text-danger"></small>
                    </div>
                  
                    <div class="form-check d-flex justify-content-start mb-4">
                      <input class="form-check-input me-2" type="checkbox" value="" name="sub" id="sub"  />
                      <label class="form-check-label" for="sub">
                          Recibir notificaciones al correo
                      </label>
                    </div>
                    
                    <div class="d-flex justify-content-center">
                        <button id="btn-register" type="submit" class="btn btn-secondary btn-block mb-4">Registrarse</button>
                    </div>
                    <div class="d-flex justify-content-center">
                        <p class="regis"><a href="../paginas/login.php">
                                ¿Ya estás registrado?
                        </a></p>
                    </div>
                </form>
            </div>
            
          </div>
  
        </div>
      </section>

    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <img src="../assets/img/kitcheeslogo.png" width="220" height="70">
                        <p>
                            Mall de Dubai <br>
                            Financial Center Street<br>
                            Dubai - Emirates Árabes <br><br>
                            <strong>Teléfono:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> kitcheesBCN@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Links útiles</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#hero">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#about">Sobre nosotros</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Servicios</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#faq">Preguntas frecuentes</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#contact">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestros Servicios</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Acceder a recetas de todo el
                                    mundo</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poder crear tus propias
                                    recetas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poseer una lista de recetas
                                    favoritas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Generación de receta aleatoria y
                                    mucho más</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestras Redes Sociales</h4>
                        <p>Síguenos en todas nuestras redes! <br>Te esperamos!</p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <script src="../assets/vendor/aos/aos.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="../assets/vendor/php-email-form/validate.js"></script>

    <script src="../assets/js/main.js"></script>
</body>

<?php

echo "<script>var hola=\"".$_GET["error"]."\";</script>";

?>

<script>
    document.getElementById("btn-register").addEventListener("click", comprovarFormulari)

    if(document.getElementById("sub").checked===false){
        document.getElementById("sub").value= "0";
    }

    if (hola == "false"){
        document.getElementById("errorUsuario").innerHTML = "El nombre de usuario ya existe!"
    } 
    
    function comprovarFormulari(event){
        event.preventDefault();

        var arrayValidacio = document.getElementsByTagName("small");
        for(var validacio of arrayValidacio){
            validacio.innerHTML = "";
        };

        var arrayBorder = document.getElementsByTagName("input");
        for(var borde of arrayBorder){
            borde.style.borderColor= "gray";
        };

        if((validacioUsuario() && validacioNombre() && validacioEmail() && validacioPassword() && validacioFoto()) == true){
            document.getElementById("formulario").submit();
        }
    }Password

    function validacioUsuario(){
        var usuario = document.getElementById("nombreUsuario");

        if(usuario.value.length < 3 || usuario.value.length > 10){
            document.getElementById("errorUsuario").innerHTML = "¡El nombre de debe contener entre 3 y 10 caracteres!"
            document.getElementById("nombreUsuario").style.borderColor = "red";
            return false;
        }else if(usuario.value.includes(" ")){
            document.getElementById("errorUsuario").innerHTML = "¡El nombre de usuario no puede contener un espacio en blanco!"
            document.getElementById("nombreUsuario").style.borderColor = "red";
            return false;
        }
        return true;            
        
    }
    
    function validacioNombre(){
        var nombre = document.getElementById("nombre");

        if(nombre.value.length == 0){
            document.getElementById("errorNombre").innerHTML = "¡Escribe un nombre!"
            document.getElementById("nombre").style.borderColor = "red";
            return false; 
        }
        return true;
    }


    function validacioEmail(){
      var emailCorrector = /^[a-zA-Z0-9.!#$%&"*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      var errorElement = document.getElementById("errorEmail");

      if (!email.value.match(emailCorrector)) {
          document.getElementById("errorEmail").innerHTML = "¡Escribe un email válido!";
          document.getElementById("email").style.borderColor = "red";
          return false;
      }
        return true;
    }


    function validacioPassword(){
        var password = document.getElementById("contra");

        if(password.value.length == 0){
            document.getElementById("errorPassword").innerHTML = "Escribe una contraseña!"
            document.getElementById("contra").style.borderColor = "red";
            return false; 
        }else if(password.value.includes(" ")){
            document.getElementById("errorUsuario").innerHTML = "La contraseña no puede contener espacios en blanco!"
            document.getElementById("contra").style.borderColor = "red";
            return false;
        }
        return true;
    }

    function validacioFoto(){
        var pfp = document.getElementById("foto");

        if(pfp.value.length == 0){
            document.getElementById("errorFoto").innerHTML = "Selecciona una foto de perfil!"
            document.getElementById("foto").style.borderColor = "red";
            return false; 
        }
        return true;
    }
    
</script>

</html>