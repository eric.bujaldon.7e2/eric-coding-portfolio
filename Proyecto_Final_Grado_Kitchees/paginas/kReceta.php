<?php
include("kModel.php");

$receta1 = new Receta();
$receta1->nombre = "Panecillos daneses semi-integrales de zanahoria y semillas de girasol | Gulerodsbrud";
$receta1->categoria = "Dulce";
$receta1->pais = "Dinamarca";
$receta1->descripcion = "Estos panecillos daneses son la locura, de aspecto rústico";
$receta1->ingredientes = ['agua tibia', 'levadura fresca', 'azúcar','harina de trigo', 'harina integral', 'sal gruesa', 'mantequilla sin sal', 'zanahoria rallada', 'semillas de girasol', 'huevo', 'trigo integral para amasar'];
$receta1->ingredientes2 = ['200 ml', '30 gr', '30 gr','375 gr', '45 gr', '3 gr', '50 gr', '300 gr', '250 gr', '1', '120gr'];
$receta1->elaboracion = "1. Diluir la levadura fresca en el agua tibia, adicionar dos cucharadas de harina de la medida total, adicionar el azúcar, remover bien y dejar reposar unos 10 minutos hasta que se forme espuma o se active la levadura.
2. Aparte en un bol, tamizar las harinas, adicionar la sal gruesa y remover.
3. Una vez activada la levadura, adicionarla a las harinas, remover con cuchara un poco y luego agregar la mantequilla cortada en trozos, comenzar a amasar hasta tener una masa homogénea. Formar una bola, cubrir con un paño y dejar levar por 1 hora..
4. Pelar las zanahorias y rallarlas...
5. Una vez levada la masa, espolvorear un poco de harina integral en una superficie plana y volcar la masa, formar un pequeño hueco y adicionar por partes las semillas de girasol, la zanahoria rallada, el huevo, cerrar la masa y comenzar a amasar de nuevo, acá la masa se volverá completamente pegajosa, por lo que es importante tener a mano harina de trigo integral
6. Adicionar un poco de harina de trigo integral e intentar compactar la masa, adicionar las semillas restantes y amasar a los pocos, con ayuda de una rasqueta de panadería metálica despegar la masa que quede en la mesa, limpiarnos las manos, espolvorear un poco más de harina...
7. En total adicioné de más 100 gr de harina de trigo integral, formar una bola. Alistar una bandeja con papel para horno y pincelar con aceite de oliva
8. Aplanar un poco la bola que formamos, cortar por mitades hasta conseguir 8 partes, (acá es importante manejar cada parte con las manos húmedas, porque la masa es pegajosa). Formar bolitas y disponerlas en la bandeja, es importante dejarlas con unos 3 cm de distancia porque crecen bastante. Dejar levar por 1 hora, cubriendo las bandejas con un paño de algodón. (Utilicé una bandeja grande y otra para dos panes restantes, en total salen 8 de unos 126 gr. aprox.)
9. Precalentar el horno media hora antes de hornear a 220ºC.
10. Acá pueden ver que después del tiempo del segundo levado crecen bastante. Poner en la parte de abajo del horno una bandeja con agua hirviendo (para hornearlos con vapor y que queden tiernos). Llevarlos a hornear en la mitad del horno por 20-25 minutos, hasta que estén dorados.
11. Una vez horneados sacar del horno, dejar reposar un poco y con cuidado quitarlos del papel y ponerlos sobre una rejilla a enfriar.";
$receta1->dificultad = "Facil";
$receta1->addFoto("https://4.bp.blogspot.com/-CVfPk3p4xJE/Wsvn6fryUjI/AAAAAAAAM48/Y3TVft1-MpMHf4mEvN7NmhsF0ujcqpwewCLcBGAs/s1600/panecillos%2Bzanahoria%2B20.JPG");
$receta1->addFoto("https://4.bp.blogspot.com/-4piSuFKczGQ/Wsvn7vpA8yI/AAAAAAAAM5Q/cRRI3uMg1hc01MnRJI50wK7nVoNbgfg7QCLcBGAs/s1600/panecillos%2Bzanahoria%2B27.JPG");
$receta1->addFoto("https://3.bp.blogspot.com/-1nWxZkOKikM/Wsvn6_e7zxI/AAAAAAAAM5I/XxKjXhLI0ikHHwKYG8UdI33OKmfiDdvawCLcBGAs/s1600/panecillos%2Bzanahoria%2B25.JPG");
$receta1->addFoto("https://i.pinimg.com/736x/28/98/8b/28988b15e06371b564c5c08d6b19a2d0.jpg");


$receta2 = new Receta();
$receta2->nombre = "ALBÓNDIGAS DANESAS | FRIKADELLEN";
$receta2->categoria = "Carnes";
$receta2->pais = "Dinamarca";
$receta2->descripcion = "Unas albóndigas que quedan de vicio. Son súper jugosas y la salsa es muy sencillita de hacer, rápida y súper sabrosa.";
$receta2->ingredientes = ['carne picada', 'cebolla', 'pan de molde','leche', 'perejil picado', 'harina', 'dientes de ajo', 'pimentón', 'Pimienta', 'Sal', 'Aceite de oliva', 'caldo de carne', 'ajo en polvo', 'mantequilla'];
$receta2->ingredientes2 = ['1/2 kilo', '1', '2 reb', '1/2 vaso', '1 puñ', '2 cuch', '2 dientes', '1/2 cucharadita', 'Al gusto', 'Al gusto', 'Al gusto', '1 vaso y 1/2', '1 pizca', '1 cucharadita'];
$receta2->elaboracion = "Vamos con las albóndigas. Ponemos el pan en un bol y añadimos la leche; dejamos que se empape. Mientras tanto, picamos finamente la cebolla y los dientes de ajo y los sofreímos en una sartén con dos cucharadas de aceite hasta que estén bien pochados y un poco doraditos.

Retiramos del fuego y dejamos templar. En un bol amplio ponemos la carne, el huevo, el pan escurrido y sal, pimienta y pimentón al gusto. Añadimos también la verdura pochada y mezclamos bien con las manos. Formamos bolas (yo las he hecho un poco más grandes que las albóndigas tradicionales), las enharinamos ligeramente y las freímos en tandas en un sartén con no demasiado aceite. Cuando estén doradas, las pasamos a una fuente con papel de cocina para escurrir el exceso de grasa.

Ahora vamos con la salsa. Ponemos el aceite y la mantequilla en una cazuela y cuando esté caliente añadimos la harina. Sofreímos unos minutos, hasta que coja un poco de color y echamos el caldo poco a poco, mezclando con las varillas, hasta tener una salsa ligada pero no demasiado espesa. Sazonamos con perejil, pimienta, un poco de ajo en polvo y una pizca de sal si es necesario y añadimos las albóndigas.

Dejamos al chup chup 12-15 minutos, hasta que las albóndigas terminen de hacerse y los sabores se unifiquen. Probamos para rectificar de sal y servimos bien caliente con un poco más de perejil espolvoreado y bien de pan para mojar en la salsa. ¡Que aproveche!";
$receta2->dificultad = "Facil";
$receta2->addFoto("https://4.bp.blogspot.com/-GVs92Rpkv3I/Wsx-ZvWbw9I/AAAAAAAAV3o/we0yd8GZPd4MK8904aw2pxRg6WBVmEC5wCLcBGAs/s1600/FRIKADELLEN.JPG");
$receta2->addFoto("https://4.bp.blogspot.com/-lpvTC12MG-Q/Wsx-7Dy8bOI/AAAAAAAAV38/lOXQKfHHp8wqgC-4uNlHctp3e2wY0y2bgCLcBGAs/s1600/FRIKADELLEN%2B%25285%2529.JPG");
$receta2->addFoto("https://2.bp.blogspot.com/-8Bkm7M9U_XA/Wsx-ljhlGBI/AAAAAAAAV3w/P_Bb0N-UWZg17l1DJRDAuMIxqZtwA0JsgCLcBGAs/s1600/FRIKADELLEN%2B%25283%2529.JPG");
$receta2->addFoto("https://4.bp.blogspot.com/-6XFJNzppWp0/Wsx-ynocVfI/AAAAAAAAV30/QADQ-VtEnOYcXT0sHUQiispR3f8V1bizQCLcBGAs/s1600/FRIKADELLEN%2B%25286%2529.JPG");


$receta3 = new Receta();
$receta3->nombre = "Antipasti";
$receta3->categoria = "Entrantes";
$receta3->pais = "Italia";
$receta3->descripcion = "Una especie de entrante que hace las funciones de aperitivo antes del plato principal.";
$receta3->ingredientes = ['Berenjena grande', 'Aceite de oliva virgen extra', 'Apio','Cebolla', 'Pimiento amarillo', 'Pimiento rojo', 'Pimiento verde', 'Tomate', 'Vinagre', 'Azúcar', 'Aceitunas', 'Alcaparras', 'Sal', 'Anchoas'];
$receta3->ingredientes2 = ['1', '120 ml', '2', '1', '1/2', '1/2', '1/2', '400 g', '30 ml', 'Al gusto', '125 g', '3 cuch', 'Al gusto', 'Al gusto'];
$receta3->elaboracion = "Cortamos en dados la berenjena y la ponemos con una cucharada de sal a drenar una hora encima de un papel de cocina o un colador. Secamos con más papel y la reservamos. Picamos en trozos iguales el resto de las hortalizas.

En una cazuela calentamos un poco de aceite de oliva y salteamos la berenjena seis minutos. La retiramos a una fuente, bajamos el fuego y añadimos un poco más de aceite. Salteamos el apio y retiramos. Añadimos otra cucharada de aceite y agregamos los pimientos con una pizca de sal y los cocinamos seis minutos, pasándolos de nuevo a la fuente.

Agregamos otra cucharada de aceite y salteamos la cebolla cinco minutos, aumentamos el fuego y echamos los tomates en dados, dejamos cocinar cinco minutos y agregamos el vinagre y el azúcar. Cocemos unos ocho minutos, reincorporando todos los vegetales junto con las aceitunas en trozos y las alcaparras. Incorporamos las anchoas picadas, dejamos cinco minutos y retiramos para que se enfríe. Es recomendable refrigerar antes de servir.";
$receta3->dificultad = "Facil";
$receta3->addFoto("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVuX8Wg0BKouTTN_v6w4HGHH1olEKQafBp9Q&usqp=CAU");
$receta3->addFoto("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNxfLg8zQmQX4pBt7BkDEQS0U-ZczB5O1pfQ&usqp=CAU");
$receta3->addFoto("https://media-cdn.tripadvisor.com/media/photo-s/13/d9/48/e1/antipasto-italiano-con.jpg");
$receta3->addFoto("https://chicastrendy.com/wp-content/uploads/2016/03/antipasto-vegetariano-Bruschetta-963x1024_opt.jpg");


$receta4 = new Receta();
$receta4->nombre = "Lasaña de carne a la boloñesa";
$receta4->categoria = "Pasta";
$receta4->pais = "Italia";
$receta4->descripcion = "La lasaña de carne a la boloñesa es una receta clásica de la cocina italiana.";
$receta4->ingredientes = ['Salsa boloñesa', 'Láminas de pasta para lasaña', 'Queso rallado para gratinar','Mantequilla', 'Harina de trigo', 'Leche', 'Sal', 'Pimienta blanca molida', 'Nuez moscada molida'];
$receta4->ingredientes2 = ['750 g', '12', '100 g', '70 g', '70 g', '1 l', 'Al gusto', 'Al gusto', 'Al gusto'];
$receta4->elaboracion = "Partimos de la base que tenemos lista nuestra boloñesa con la que rellenar la lasaña. Existen infinidad de recetas de esta famosa elaboración italiana, pero nosotros os recomendamos la receta de salsa boloñesa de Orson Welles que, aunque poco ortodoxa, es una auténtica delicia. No obstante, podéis usar la salsa boloñesa tradicional auténtica o la salsa que más os guste.

Preparamos nuestra salsa bechamel casera y, para ello, colocamos la mantequilla en un cazo y la calentamos hasta fundir. Agregamos la harina, removemos para que se mezcle bien con la mantequilla y se tueste durante un par de minutos. Vertemos poco a poco la leche caliente, y sin dejar de remover, cocemos durante 20 minutos hasta que espese. Salpimentamos y añadimos la nuez moscada al gusto.

Cocemos las láminas de pasta para lasaña siguiendo las instrucciones del paquete. Algunas marcas solo requieren de un baño en agua caliente, otras de un hervor en condiciones y unas terceras no necesitan de ninguna de las dos cosas anteriores pues se hidratan en el horno al contacto con el relleno y la salsa bechamel que, en este caso, se prepara más líquida de lo normal.

Para montar la lasaña colocamos en el fondo de una fuente apta para horno un poco de salsa bechamel, encima una capa de láminas de lasaña que cubrimos con el relleno de boloñesa. Regamos con un poco de salsa bechamel y colocamos nuevamente láminas de lasaña, boloñesa y así hasta llegar casi al borde de la fuente.

Cubrimos la última capa, que será de láminas de lasaña, con salsa bechamel. Espolvoreamos con queso rallado y horneamos durante 15-20 minutos o hasta que el queso se haya fundido y adquirido un color dorado. Si en lugar de hornear nos decidimos por gratinar, entonces el tiempo se reduce bastante así que conviene estar atentos al horno para que no se queme la superficie.

Una vez lista nuestra lasaña la retiramos del horno y dejamos que repose unos minutos antes de servir. Conviene no esperar mucho para que no pierda jugosidad, pero tampoco hay que ser demasiado rápidos si no queremos quemarnos pues sale muy caliente del horno.";
$receta4->dificultad = "Media";
$receta4->addFoto("https://www.recetasdesbieta.com/wp-content/uploads/2018/10/lasagna-original..jpg");
$receta4->addFoto("https://cdn.colombia.com/gastronomia/2011/08/25/lasagna-3685.jpg");
$receta4->addFoto("https://www.deliciosi.com/images/2100/2181/lasa%C3%B1a-a-la-bolo%C3%B1esa.jpg");
$receta4->addFoto("https://recetasd.com/wp-content/uploads/2013/01/lasana-ita_13505610218712-749x320.jpg");


$receta5 = new Receta();
$receta5->nombre = "Causa rellena";
$receta5->categoria = "Pescados";
$receta5->pais = "Peru";
$receta5->descripcion = "La causa rellena ó causa limeña, es uno de los platos más fabulosos que tiene la gastronomía peruana.";
$receta5->ingredientes = ['papas amarillas andinas grandes', 'Láminas de pasta para lasaña', 'Queso rallado para gratinar','Mantequilla', 'Harina de trigo', 'Leche', 'Sal', 'Pimienta blanca molida', 'Nuez moscada molida'];
$receta5->ingredientes2 = ['750 g', '12', '100 g', '70 g', '70 g', '1 l', 'Al gusto', 'Al gusto', 'Al gusto'];
$receta5->elaboracion = "La elaboración de la causa rellena requiere de varios pasos y, si bien toma un tiempo y tiene un proceso a seguir, es muy fácil de hacer.
Primero debes dejar todo pelado y cortado:
Pica la cebolla y el tomate en cubos pequeños y reserva en un bowl.
Corta el aguacate en dados medianos. Reserva.
Sancocha un huevo, quítale la cáscara y reserva.
Pica el ají amarillo en cuadritos muy pequeños y reserva.
Pela y pica las papas en trozos medianos y ponlas a hervir en una olla con agua y una pizca de sal.
Mientras se hacen las papas puedes preparar el relleno de la causa limeña:
Mezcla el tomate, la cebolla, un parte del ají amarillo, las latas de atún y la mayonesa. Agrégale una pizca de sal.
Si ya están listas las papas, ponlas en un recipiente grande y hazlas puré aplastándolas con un tenedor o con un respectivo pisapapas. Agrega un chorro de aceite de oliva y el jugo del medio limón. Sigue aplastando y mezclando.
También puedes agregarle algo de ají amarillo, preferiblemente molido hasta que se haga pasta. Mezcla todo muy bien hasta que tome una buena consistencia de puré. No debe estar muy cremoso sino más bien un poco compacto.
Ahora llega el momento de armar tu maravillosa causa rellena.
Lo ideal es que tengas un molde redondo donde puedas ir agregando las capas.
Empieza primero con una capa de puré de papas, procura que tenga de grosor unos 2 centímetros, aunque esto al final va a depender un poco de ti.
Luego agrega una capa del relleno preparado previamente. Ayúdate con un tenedor y/o una cuchara para que quede compacto.
Seguidamente agrega una última capa de puré de papas, utiliza nuevamente un tenedor o cuchara para compactar y darle forma.
En este momento puedes agregar el topping de aguacate (mezclado con un poquito de mayonesa si quieres) y encima unas ruedas de huevo sancochado.
También puedes hacerlo en otro orden y, luego de agregar el relleno, poner el aguacate y finalmente el puré de papas.
Puedes colocar algunos trozos de ají amarillo por encima de tu causa limeña a modo de decoración.
También puedes ponerle algo de ají (en crema) a un lado del plato.
Puedes ponerle un poco de perejil fresco por encima y algunas aceitunas negras sin hueso.
Listo, ya tienes una deliciosa causa rellena de atún.";
$receta5->dificultad = "Fácil";
$receta5->addFoto("http://www.comedera.com/wp-content/uploads/2015/09/causa-rellena-de-atun.jpg");
$receta5->addFoto("https://www.aliexperu.com/storage/2019/03/causa-de-pollo-con-mayonesa.jpg");
$receta5->addFoto("https://cdn1.cocina-familiar.com/recetas/thumb/causa-limena-de-atun.jpg");
$receta5->addFoto("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMcPe2Ow3cIsyiSeg_zFRPTEO5cH-de1_chg&usqp=CAU");


$receta6 = new Receta();
$receta6->nombre = "Salchipapa";
$receta6->categoria = "Carnes";
$receta6->pais = "Peru";
$receta6->descripcion = "Aprende cómo preparar salchipapas al estilo callejero, con sus salsas incluidas. Una comida fácil, rápida y deliciosa.";
$receta6->ingredientes = ['papas grandes', 'salchichas', 'Sal','Aceite para freír', 'Ketchup', 'Mostaza', 'Mayonesa'];
$receta6->ingredientes2 = ['4', '6', 'Al gusto', 'Al gusto', 'Al gusto', 'Al gusto', 'Al gusto'];
$receta6->elaboracion = "Pela las papas y córtalas en bastones, al estilo de las papas fritas de toda la vida. Y es que eso es justo los que vas a hacer a continuación.
Agrega abundante aceite vegetal en una sartén y freír las papas hasta que estén doradas y crujientes.
Retira de la sartén, colócalas en papel absorbente para reducirles la grasa. Agrégales sal al gusto.
Pica las salchichas en varias piezas de aproximadamente 4 centímetros de largo. En realidad el tamaño es a tu gusto.
En otra sartén pon un chorro pequeño de aceite y cocina las salchichas hasta que estén asadas por todas sus caras vigilando que no se vayan a quemar mucho.
Ahora simplemente mezcla en un plato las salchichas y las papas fritas y échales las tres salsas: Ketchup, mostaza y mayonesa a tu gusto.
Listo, ya puedes disfrutar de unas deliciosas salchipapas.";
$receta6->dificultad = "Fácil";
$receta6->addFoto("https://imag.bonviveur.com/emplatado-final-de-las-salchipapas.jpg");
$receta6->addFoto("https://www.comedera.com/wp-content/uploads/2021/07/salchipapas.jpg");
$receta6->addFoto("https://e.rpp-noticias.io/normal/2019/05/14/513851_789110.jpg");
$receta6->addFoto("https://www.totusnoticias.com/wp-content/uploads/2021/12/Receta-de-Salchipapas-Salchipapa-colombiana-%E2%80%93-La-Cocina-Latina.jpg");


$receta7 = new Receta();
$receta7->nombre = "Avgolemono | Sopa griega de limón";
$receta7->categoria = "Sopas";
$receta7->pais = "Grecia";
$receta7->descripcion = "La avgolemono es un plato muy típico del Mediterráneo oriental";
$receta7->ingredientes = ['Zumo colado de limones', 'cáscara rallada de un limón', 'caldo de pollo desgrasado','azafrán molido', 'crema de leche', 'yemas de huevo', 'mostaza en polvo'];
$receta7->ingredientes2 = ['1,5 l', 'Al gusto', '1,25 l', 'Al gusto', '150 cc', '2', 'Al gusto'];
$receta7->elaboracion = "Calienta el caldo de pollo hasta que hierva. Añade el azafrán y el zumo de limón, colado. Separa del fuego y deja reposar unos 30 minutos, hasta que entibie. Bate la crema de leche con las yemas y la mostaza en polvo. Añade a la sopa tibia, remueve bien con batidora o varillas, para amalgamar los ingredientes de la sopa. Deja en frío hasta el momento de servir, y decora con la cáscara de limón rallada. Al llevar yemas de huevo debemos recordar que una vez hecha, si la queremos calentar, no debe hervir, ya que se cortaría.";
$receta7->dificultad = "Fácil";
$receta7->addFoto("https://www.goya.com/media/6661/avgolemono5.jpg?quality=80");
$receta7->addFoto("https://bakingthegoods.com/wp-content/uploads/2018/02/Avgolemono-Soup-17.jpg");
$receta7->addFoto("https://www.seriouseats.com/thmb/Dw0D-Nxokzggxbq_KmmdsNw2lAc=/450x0/filters:no_upscale():max_bytes(150000):strip_icc()/__opt__aboutcom__coeus__resources__content_migration__serious_eats__seriouseats.com__2018__04__20180208-avgolemono-vicky-wasik-10-b4a179f376574aaab84d91c82382dbfe.jpg");
$receta7->addFoto("https://static.onecms.io/wp-content/uploads/sites/9/2013/12/06/200903-r-xl-avgolemono-chicken-soup-with-rice.jpg");


$receta8 = new Receta();
$receta8->nombre = "Sartén de ensalada griega";
$receta8->categoria = "Ensaladas";
$receta8->pais = "Grecia";
$receta8->descripcion = "Nada sorprende en estos ingredientes, pero sí en la forma de prepararlos, ya que se puede enriquecer la receta añadiendo huevo batido y dándole un punto de calor, que levanta los aromas de las hortalizas y también nos permite 'sacar' del verano a este clásico griego.";
$receta8->ingredientes = ['huevos', 'cebolleta', 'tomate cherry','aceituna negra', 'queso feta', 'aceite de oliva', 'sal', 'perejil fresco'];
$receta8->ingredientes2 = ['8', '1', '120g', '100g', '100g', 'Al gusto', 'Al gusto', 'Al gusto'];
$receta8->elaboracion = "Comenzaremos batiendo los huevos con un pellizco de sal y una cucharada de perejil picado. Picamos la cebolleta menuda y seguidamente colocamos una sartén antiadherente al fuego y añadimos un chorrito de aceite de oliva, sofreímos la cebolleta cuatro minutos hasta que comience a estar dorada. Añadimos los tomates cherry partidos a la mitad y las aceitunas negras y cocinamos durante otros dos minutos hasta que los tomates empiecen a verse blanditos. Encendemos el grill del horno. Reducimos el fuego y añadir los huevos batidos, cocinamos durante cinco minutos hasta que veamos los huevos cuajados. Añadimos el queso feta desmenuzado por encima y gratinamos durante cinco minutos hasta que veamos la superficie dorada. Espolvoreamos con un poco más de perejil fresco.";
$receta8->dificultad = "Fácil";
$receta8->addFoto("https://www.lazarola.com/wp-content/uploads/2018/08/f07cd0e2-8f1e-481b-8b92-987a77af4bfa.jpg");
$receta8->addFoto("https://i.blogs.es/04ebe7/sarten-20griega-dap/1366_521.jpg");
$receta8->addFoto("https://www.recetasmetodogrez.com/wp-content/uploads/2018/07/ensalada-griega-sarten.jpg");
$receta8->addFoto("https://imagenes.20minutos.es/files/image_656_370/files/fp/uploads/imagenes/2021/03/29/sarten-de-ensalada-griega.r_d.600-400-8051.jpeg");


$receta9 = new Receta();
$receta9->nombre = "Quiche Lorraine";
$receta9->categoria = "Postres";
$receta9->pais = "Francia";
$receta9->descripcion = "La receta de quiche Lorraine es el plato estrella de cualquier celebración, porque es muy fácil de preparar y está deliciosa tanto fría como caliente.";
$receta9->ingredientes = ['Harina de trigo', 'Mantequilla', 'Huevos', 'Sal', 'Crema de leche', 'Queso rallado', 'Champiñones', 'Jamón de York', 'Pimienta negra molida', 'Nuez moscada molida'];
$receta9->ingredientes2 = ['200 g', '100 g', '4', 'Al gusto', '200 ml', '150 g', '100 g', '120 g', 'Al gusto', 'Al gusto'];
$receta9->elaboracion = "Empezamos preparando la masa quebrada. Hacemos un volcán con la harina, añadimos la mantequilla cortada en dados y vamos uniendo todo hasta conseguir una masa homogénea. No conviene que amasemos demasiado para que no nos quede una masa demasiado dura. Hacemos una bola, la envolvemos con papel film y la guardamos media hora en el frigorífico.

Pasado este tiempo, extendemos la masa con el rodillo y cubrimos con ella un molde acanalado de base desmoldable. Pasamos el rodillo por el borde y retiramos el sobrante de masa. Pinchamos varias veces con un tenedor y horneamos la base durante 15 minutos en el horno precalentado a 180ºC.

Mientras, rallamos el queso, cortamos el jamón en daditos, fileteamos los champiñones y los doramos ligeramente en una sartén para que pierdan el agua. En un bol grande batimos los huevos y los mezclamos con la nata y la leche, la sal, la pimienta y una pizca de nuez moscada.

Sobre la base de la tarta ponemos el jamón, el queso y los champiñones. Sobre ellos echamos la mezcla de nata y huevo, dejando que se empape bien todo. Por último, horneamos la quiche a 180º durante 30 o 40 minutos o hasta que esté el relleno cuajado y la masa dorada. Servir caliente.";
$receta9->dificultad = "Fácil";
$receta9->addFoto("https://i0.wp.com/irecetasfaciles.com/wp-content/uploads/2019/02/quiche-lorraine-una-receta-francesa.jpg?fit=900%2C632&ssl=1");
$receta9->addFoto("https://cdn.kiwilimon.com/recetaimagen/1705/th5-640x640-7209.jpg");
$receta9->addFoto("https://www.foodspring.es/magazine/wp-content/uploads/2020/11/protein_quiche_rezept-2.jpg");
$receta9->addFoto("https://saposyprincesas.elmundo.es/wp-content/uploads/2017/10/quiche-lorraine.jpg");


$receta10 = new Receta();
$receta10->nombre = "Crepe";
$receta10->categoria = "Dulces";
$receta10->pais = "Francia";
$receta10->descripcion = "En esta receta vamos a ver cómo preparar la masa básica de crepes, a la que después podrás añadir ingredientes de tu gusto.";
$receta10->ingredientes = ['Harina', 'leche', 'azúcar', 'mantequilla para untar', 'huevos', 'mantequilla', 'sal'];
$receta10->ingredientes2 = ['125 gr', '250 ml', '5 gr', '1 cucharadita', '2', '50 gr', '1 pizca'];
$receta10->elaboracion = "En primer lugar, funde la mantequilla introduciéndola en el microondas durante 30 0 40 segundos.

A continuación, pon en un vaso de batidora los ingredientes líquidos: la leche, la mantequilla fundida y los huevos. Añade la harina, el azúcar y la sal. Tritura todos los ingredientes hasta conseguir una crema y pasa la mezcla por un colador para eliminar cualquier grumo que pueda tener. También puedes mezclarlos a mano con las varillas.

Unta una sartén antiadherente con una cucharadita de mantequilla con la ayuda de un pincel o brocha. Pon a calentar la sartén a fuego medio.

Vierte un poco de masa en el centro y espárcela bien por toda la sartén. Cuando empiece a cuajarse, dale la vuelta y cocina el crepe brevemente por el otro lado.

Repite la operación hasta terminar toda la masa.

¡Los crepes ya están listos!";
$receta10->dificultad = "Fácil";
$receta10->addFoto("https://annaspasteleria.com/images/2019post/_videoCover/IMG_7029editweb.jpg");
$receta10->addFoto("https://www.cocinaabuenashoras.com/files/como-hacer-masa-de-crepes-facil-y-rapida.jpg");
$receta10->addFoto("https://www.kenwoodworld.com/Global/Countries/Spain/Recetas%20kMix/21152_Mini%20crepes.jpg");
$receta10->addFoto("https://saboryestilo.com.mx/wp-content/uploads/elementor/thumbs/Crepas-Jamon-Queso-p8dtre8kt04jn3s7heph9ip2tncnkhs9xy3n096rsw.jpg");


$receta11 = new Receta();
$receta11->nombre = "Pinchos morunos";
$receta11->categoria = "Carnes";
$receta11->pais = "Marruecos";
$receta11->descripcion = "En esta receta vamos a ver cómo preparar la masa básica de crepes, a la que después podrás añadir ingredientes de tu gusto.";
$receta11->ingredientes = ['carne de cordero', 'cebolla', 'aceite', 'vinagre', 'perejil', 'cilantro fresco', 'cilantro seco', 'comino', 'pimienta', 'pimentón dulce', 'sal'];
$receta11->ingredientes2 = ['2 kg', '1', '1 cuchara', '1 cucharadita', '3 cucharas', '1 cuchara', '1/2 cucharita', '1/2 cucharita', '1/2 cucharita', '1 cucharita y 1/2', 'Al gusto'];
$receta11->elaboracion = "Como ya he comentado, el truco para que estos pinchos morunos queden bien sabrosos es dejar la carne un día en adobo. Para preparar el adobo vamos a mezclar en un recipiente una taza de aceite de oliva, la cebolla previamente pelada y picada, los dientes de ajo pelados y picados, un cuarto de cuchara de pimienta, otro de jengibre, otro de comino, otro de cúrcuma, otro de pimentón picante y una pizca de sal.
Una vez tengamos listo el adobo, cortaremos la carne de cordero en tacos medianos y los pondremos en el recipiente con el adobo. Mezclamos bien para que toda la carne se impregne y lo meteremos en el frigorífico, donde tendrá que estar durante 24 horas. Cada 8 horas removemos la carne de cordero para que absorba bien la mezcla de especias.
Al día siguiente pinchamos los trozos de carne en las varillas formando los pinchos morunos. Seguidamente los pasamos por la plancha o una sartén con unas gotitas de aceite de oliva hasta que quede la carne a nuestro gusto. Retiramos y servimos los pinchos de cordero con una ensalada para complementar el plato. Buen provecho!!";
$receta11->dificultad = "Fácil";
$receta11->addFoto("https://i.blogs.es/3fc4d6/pinchos-morunos/840_560.jpg");
$receta11->addFoto("https://cdn.elcocinerocasero.com/imagen/receta/1000/2018-06-06-18-51-10/pinchos-morunos-caseros.jpeg");
$receta11->addFoto("https://ep01.epimg.net/elcomidista/imagenes/2020/09/02/receta/1599057324_976507_1599058656_media_normal.jpg");
$receta11->addFoto("https://1.bp.blogspot.com/-990W7KxH2GU/XsQYpLUeOsI/AAAAAAAAZ1M/L6Onpeaq00oMFD-qb95JoJq57r0Rk4L_gCLcBGAsYHQ/s1600/pinchos%2Bmorunos%2Bcaseros%2B%25287%2529.jpg");


$receta12 = new Receta();
$receta12->nombre = "Cuscus";
$receta12->categoria = "Arroces";
$receta12->pais = "Marruecos";
$receta12->descripcion = "En esta receta vamos a ver cómo preparar la masa básica de crepes, a la que después podrás añadir ingredientes de tu gusto.";
$receta12->ingredientes = ['carne picada de ternera', 'cebolla pequeña', 'chile verde fresco', 'dientes de ajo', 'comino en polvo', 'chile seco', 'orégano', 'aceite de oliva', 'zumo de lima', 'sal', 'cilantro', 'tortillas de maíz'];
$receta12->ingredientes2 = ['400 ml', '300 g', '2 cucharas', '1 cucharadita', '2', '2 cucharas'];
$receta12->elaboracion = "En una olla mediana pon a hervir 400 ml de caldo de ave o caldo de verduras, o en su defecto, 400 ml de agua con 5 g de sal y dos cucharadas de aceite de oliva. CONSEJO: Si vas a hacer cous cous 1 persona emplea 100 ml de agua por cada 75 g de cuscús.
Cuando el agua comience a hervir, retira la olla del fuego e incorpora el resto de ingredientes.
Espera 5 minutos con la cazuela tapada y separa los granos con un tenedor.
Antes de servir, retira las guindillas para evitarle una sorpresa desagradable a tus comensales.";
$receta12->dificultad = "Fácil";
$receta12->addFoto("https://placeralplato.com/files/2015/10/cuscus.jpg");
$receta12->addFoto("https://recetasdecocina.elmundo.es/wp-content/uploads/2017/04/receta-cuscus.jpg");
$receta12->addFoto("https://www.sanpellegrinofruitbeverages.com/mx/sites/g/files/xknfdk901/files/couscous_4.jpg");
$receta12->addFoto("https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/D3FE131E-EB10-46E8-9E93-5E178323751D/Derivates/A0B1EE28-94D2-4922-B871-CFB63832A281.jpg");


$receta13 = new Receta();
$receta13->nombre = "Coixinhas de frango";
$receta13->categoria = "Entrantes";
$receta13->pais = "Brasil";
$receta13->descripcion = "Es una receta similar a nuestras croquetas en el sentido de que es una masa rellena, empanada y frita, pero ni la evoltura ni el relleno tienen nada que ver. Las coixinhas son crujientes por fuera y algo cremosas por dentro y se pueden hacer rellenas tanto de pollo -lo habitual dada su forma- como de otros ingredientes.";
$receta13->ingredientes = ['Harina de trigo', 'Caldo de pollo', 'Leche','Mantequilla', 'Pechuga de pollo', 'Cebolla pequeña', 'Diente de ajo', 'Huevo', 'Pan rallado', 'Salsa de tomate', 'Sal y pimienta al gusto', 'Tabasco rojo'];
$receta13->ingredientes2 = ['500g', '500ml', '500ml', '75g', '1', '1', '1', '2', 'Al gusto', 'Al gusto', 'Al gusto', 'Al gusto'];
$receta13->elaboracion = "Para el relleno, cocemos la pechuga de pollo, la dejamos enfriar y la deshacemos en hilos. En una sartén, salteamos la cebolla y el ajo muy picaditos, añadiendo después el tomate y unas gotas de tabasco. Después añadimos las hebras de pechuga de pollo cocido y mezclamos bien. Probamos el punto de sazón y picante y mientras se enfría, nos ponemos a hacer la masa de las coixinhas. Hay muchas formas de hacer la masa. Unos lo hacen con mezcla de caldo de pollo y leche -como os voy a enseñar- pero otros lo hacen incluso solamente con agua. En cuanto a la harina, también hay quien hace la masa de las coixinhas sustituyendo parte de la harina por patatas cocidas o por otros tubérculos como la mandioca. Para hacer nuestra masa, se ponen en una cacerola la leche, el caldo, la mantequilla y una pizca de sal y cuando empieza a hervir se añade de golpe la harina, removiendo para integrar los ingredientes. Una vez se obtenga una pasta que se despega del fondo, se saca de la cacerola y se amasa en la mesa hasta obtener una masa lisa a la que se da forma de cilindro y después se corta en porciones. Con cada una de esas porciones, -bien estirada- haremos círculos similares a las obleas de empanadilla para rellenarlos. Ponemos un círculo de masa en la palma de la mano, añadimos una cucharada de relleno y cerramos plegando los bordes hacia la parte superior. Una vez cerrados, alisamos la masa girándola en la encimera para darle la característica forma cónica. Finalmente, pasamos por huevo y pan rallado y freímos hasta dorarlas.";
$receta13->dificultad = "Media";
$receta13->addFoto("https://recetas.mx/wp-content/uploads/2020/09/Coxinha-de-frango.jpg");
$receta13->addFoto("https://recetas-rapidas.es/wp-content/uploads/2018/12/receta-brasil.jpg");
$receta13->addFoto("https://www.cocinayvino.com/wp-content/uploads/2018/07/15532179_m-696x464.jpg");
$receta13->addFoto("https://img.cybercook.com.br/imagens/receitas/259/coxinha-de-frango.jpg");


$receta14 = new Receta();
$receta14->nombre = "Arroz blanco estilo brasileño";
$receta14->categoria = "Arroces";
$receta14->pais = "Brasil";
$receta14->descripcion = "El arroz blanco es uno de los platillos que más se preparan en Brasil como guarnición. El secreto de esta receta consiste en freír el arroz con la cebolla y el ajo antes de agregar cualquier líquido.";
$receta14->ingredientes = ['Arroz blanco de grano largo', 'Cebolla picada', 'Ajo picado','Aceite vegetal', 'Sal', 'Agua caliente'];
$receta14->ingredientes2 = ['2 tazas', '2 cucharadas', '2 dientes', '2 cucharadas', '1 cucharadita', '4 tazas'];
$receta14->elaboracion = "Coloca el arroz en un colador y enjuágalo bien con agua fría. Reserva. Calienta el aceite en una cacerola a fuego medio. Cocina la cebolla en el aceite durante 1 minutos. Agrega el ajo y cocina, sin dejar de mover, hasta que se dore ligeramente. Agrega el arroz y la sal; cocina, sin dejar de mover, hasta que el arroz empiece a dorarse. Vierte el agua caliente sobre la mezcla y revuelve. Reduce el fuego a bajo, tapa la cacerola y cocina hasta que el agua se haya absorbido, de 20 a 25 minutos.";
$receta14->dificultad = "Fácil";
$receta14->addFoto("https://static.onecms.io/wp-content/uploads/sites/21/2016/05/03/d55b353e-ff6f-4d12-bea0-30b9a80f1b56.jpg");
$receta14->addFoto("https://allmexrecipes.com.mx/storage/images/arrroz-brasileno-con-limon-y-cilantro-9691-4048.jpg");
$receta14->addFoto("https://www.eluniversal.com.mx/sites/default/files/2020/02/18/arroz.jpg");
$receta14->addFoto("https://s1.dmcdn.net/v/Olwmx1X-2guAQNX8n/x240");


$receta15 = new Receta();
$receta15->nombre = "Chivito";
$receta15->categoria = "Arroces";
$receta15->pais = "Uruguay";
$receta15->descripcion = "Por un lado, el sándwich de Chivito o Chivito en pan, que es la que vamos a contaros a continuación. Normalmente se hace con filetes de ternera a la plancha, jamón y queso y se incluye lechuga, tomate, aceitunas o pepinillos y rodajas de huevo duro. O por otro, el Chivito en plato, en cuyo caso se sirve sin pan. En esta versión en plato se añade bacon o panceta a la plancha, cebolla caramelizada y se sustituye el huevo duro por un huevo frito colocado encima del resto de ingredientes.";
$receta15->ingredientes = ['Filetes de ternera uno o dos, finos y tiernos', 'Tomate en rodajas', 'Cogollo de lechuga unas hojas','Aceitunas sin hueso', 'Jamón de York (lonchas)', 'Queso Mozzarella o Havarti, para fundir en lonchas', 'Pan de barra tipo chapata o similar', 'Huevo duro o cocido', 'Mayonesa para untar el pan', 'Patata fritas para guarnición'];
$receta15->ingredientes2 = ['Al gusto', '1', 'Al gusto', '4', '2', '2', '1', '2', 'Al gusto', 'Al gusto'];
$receta15->elaboracion = "Coloca el arroz en un colador y enjuágalo bien con agua fría. Reserva. Calienta el aceite en una cacerola a fuego medio. Cocina la cebolla en el aceite durante 1 minutos. Agrega el ajo y cocina, sin dejar de mover, hasta que se dore ligeramente. Agrega el arroz y la sal; cocina, sin dejar de mover, hasta que el arroz empiece a dorarse. Vierte el agua caliente sobre la mezcla y revuelve. Reduce el fuego a bajo, tapa la cacerola y cocina hasta que el agua se haya absorbido, de 20 a 25 minutos.";
$receta15->dificultad = "Fácil";
$receta15->addFoto("https://ep01.epimg.net/elcomidista/imagenes/2021/07/06/receta/1625572040_373287_1625572401_media_normal.jpg");
$receta15->addFoto("https://ep00.epimg.net/elcomidista/imagenes/2021/07/06/receta/1625572040_373287_1625572312_rrss_normal.jpg");
$receta15->addFoto("https://e00-elmundo.uecdn.es/assets/multimedia/imagenes/2014/02/06/13916908686139.png");
$receta15->addFoto("https://i.pinimg.com/736x/50/73/ca/5073caad131d29052644919b834a5675.jpg");


$receta16 = new Receta();
$receta16->nombre = "Capeletis a la Caruso";
$receta16->categoria = "Pastas";
$receta16->pais = "Uruguay";
$receta16->descripcion = "Exquisita pasta en salsa a base de leche, jamón cocido, champiñón y los demás ingredientes que la convierten en un plato líder en las mesas uruguayas. Es ideal para compartir en una reunión con nuestros familiares.";
$receta16->ingredientes = ['Capeletis', 'Crema de leche', 'Leche','Extracto de carne', 'Almidón de maíz', 'Jamón cocido', 'Champiñones', 'Taza de queso rallado'];
$receta16->ingredientes2 = ['1kg', '250 cc', '1/4', '3 cucharadas', '200g', '250g', '1/2'];
$receta16->elaboracion = "Coloca el arroz en un colador y enjuágalo bien con agua fría. Reserva. Calienta el aceite en una cacerola a fuego medio. Cocina la cebolla en el aceite durante 1 minutos. Agrega el ajo y cocina, sin dejar de mover, hasta que se dore ligeramente. Agrega el arroz y la sal; cocina, sin dejar de mover, hasta que el arroz empiece a dorarse. Vierte el agua caliente sobre la mezcla y revuelve. Reduce el fuego a bajo, tapa la cacerola y cocina hasta que el agua se haya absorbido, de 20 a 25 minutos.";
$receta16->dificultad = "Fácil";
$receta16->addFoto("https://www.cocina-uruguaya.com/base/stock/Recipe/200-image/200-image_web.jpg");
$receta16->addFoto("https://www.196flavors.com/wp-content/uploads/2019/01/salsa-caruso-2-FP.jpg");
$receta16->addFoto("https://upload.wikimedia.org/wikipedia/commons/5/5c/Capelettis_a_la_Caruso.jpg");
$receta16->addFoto("https://chefdecuisineluisperrone.files.wordpress.com/2018/03/img_1ghh391-e1558960926690.jpg");


$receta17 = new Receta();
$receta17->nombre = "Gazpacho andaluz tradicional";
$receta17->categoria = "Entrantes";
$receta17->pais = "España";
$receta17->descripcion = "Esta receta de gazpacho andaluz tradicional os va a salir a la primera y os va a gustar mucho más que cualquiera de los gazpachos envasados, saliendo además bastante más económica así que... todos a la cocina para probarlo.";
$receta17->ingredientes = ['Tomate pera', 'Pimiento verde italiano', 'Pepino','Dientes de ajo', 'Aceite de oliva virgen extra', 'Pan de hogaza duro', 'Agua', 'Sal', 'Vinagre de Jerez'];
$receta17->ingredientes2 = ['1kg', '1', '1', '2', '50ml', '50g', '250ml', '5g', '30ml'];
$receta17->elaboracion = "Como os digo, en esto de los gazpachos la cosa va en gustos. Aunque en mi familia, jamás se perdona añadir cebolla o pimiento rojo a un gazpacho, la cebolla a veces se puede consentir muy picadita como guarnición -no dentro de los ingredientes triturados-, pero este punto será seguramente controvertido porque en otras casas se hará con cebolla siempre. Lo dejo a vuestro criterio. Lo del pimiento rojo es más delicado, puesto que siempre que se hacen gazpachos andaluces se debe hacer con pimiento verde que aporta un sabor peculiar que no encaja con el dulce del pimiento rojo carnoso. Digo lo mismo. Habrá casas donde se suela hacer con pimiento rojo, pues de acuerdo, pero si queréis hacerme caso, probad con mi receta y dejad el pimiento rojo para otros platos deliciosos como la ensalada de pimientos. Troceamos todos los ingredientes indicados en la proporción que os he puesto y añadimos 50 ml de aceite de oliva, 250 ml de agua de la nevera y 50 ml de vinagre de Jerez, triturando todo en la batidora de vaso o Turmix. No es necesario pelar los tomates o los pimientos porque luego lo vamos a pasar por el colador fino. Si tenéis un robot tipo Thermomix podéis poner todos los ingredientes en el vaso y triturarlos a máxima velocidad durante 4 minutos para obtener una textura perfecta. Una vez triturado, pasamos el gazpacho resultante por el colador fino, apretando con un cucharón para que quede una crema sin pieles ni semillas y lo metemos en la nevera un par de horas para que enfríe bien.";
$receta17->dificultad = "Media";
$receta17->addFoto("https://www.recetasderechupete.com/wp-content/uploads/2020/05/Gazpacho-andaluz-Ajustes-de-rechupete-2.jpg");
$receta17->addFoto("https://www.lavanguardia.com/files/article_main_microformat/uploads/2018/06/01/5e99784964d1d.jpeg");
$receta17->addFoto("https://www.hogarmania.com/archivos/201905/gazpacho-adaluz-xl-668x400x80xX.jpg");
$receta17->addFoto("http://www.comedera.com/wp-content/uploads/2015/06/gazpacho.jpg");


$receta18 = new Receta();
$receta18->nombre = "Merluza en salsa verde";
$receta18->categoria = "Pescados";
$receta18->pais = "España";
$receta18->descripcion = "La merluza en salsa verde es uno de los grandes clásicos de la gastronomía española. Que no lleve a engaño la sencillez de su elaboración ni los pocos y básicos ingredientes que requiere, pues cuando se utiliza materia prima de calidad se convierte en un plato de lujo cuyo resultado es espectacular. Aunque su origen nos lleva al País Vasco, donde le conoce también por el nombre de merluza a la vasca o merluza koskera, actualmente es un plato habitual en las mesas de todo el país. La receta básica lleva solamente merluza, pero se puede añadir almejas, guisantes, espárragos blancos o huevo duro en cuartos combinando estos ingredientes. Al gusto.";
$receta18->ingredientes = ['Merluza en rodajas', 'Cebolla', 'Dientes de ajo','Vino blanco', 'Caldo de pescado o fumet', 'Harina de trigo', 'Aceita de oliva virgen extra', 'Perejil fresco', 'Almeja fina(opcional)', 'Espárragos blancos en conserva(opcional)'];
$receta18->ingredientes2 = ['4', '125g', '2', '120ml', '200ml', '12g', 'Al gusto', 'Al gusto', 'Al gusto', '12', '4'];
$receta18->elaboracion = "Como os digo, en esto de los gazpachos la cosa va en gustos. Aunque en mi familia, jamás se perdona añadir cebolla o pimiento rojo a un gazpacho, la cebolla a veces se puede consentir muy picadita como guarnición -no dentro de los ingredientes triturados-, pero este punto será seguramente controvertido porque en otras casas se hará con cebolla siempre. Lo dejo a vuestro criterio. Lo del pimiento rojo es más delicado, puesto que siempre que se hacen gazpachos andaluces se debe hacer con pimiento verde que aporta un sabor peculiar que no encaja con el dulce del pimiento rojo carnoso. Digo lo mismo. Habrá casas donde se suela hacer con pimiento rojo, pues de acuerdo, pero si queréis hacerme caso, probad con mi receta y dejad el pimiento rojo para otros platos deliciosos como la ensalada de pimientos. Troceamos todos los ingredientes indicados en la proporción que os he puesto y añadimos 50 ml de aceite de oliva, 250 ml de agua de la nevera y 50 ml de vinagre de Jerez, triturando todo en la batidora de vaso o Turmix. No es necesario pelar los tomates o los pimientos porque luego lo vamos a pasar por el colador fino. Si tenéis un robot tipo Thermomix podéis poner todos los ingredientes en el vaso y triturarlos a máxima velocidad durante 4 minutos para obtener una textura perfecta. Una vez triturado, pasamos el gazpacho resultante por el colador fino, apretando con un cucharón para que quede una crema sin pieles ni semillas y lo metemos en la nevera un par de horas para que enfríe bien.";
$receta18->dificultad = "Fácil";
$receta18->addFoto("https://i.blogs.es/ba0740/merluza_salsa_verde/1366_2000.jpg");
$receta18->addFoto("https://www.divinacocina.es/wp-content/uploads/Merluza-en-salsa-verde-la-raza.jpg");
$receta18->addFoto("https://www.miscosillasdecocina.com/wp-content/uploads/2019/04/MERLUZA-SALSA-VERDE.jpg");
$receta18->addFoto("https://www.eltoquedeines.es/wp-content/uploads/2017/13/eltoquedeines_76964303-9d0a-41fc-aa4b-fa1d056c4728-e1509652228237-768x576.jpeg?v=1613559289");


$receta19 = new Receta();
$receta19->nombre = "Tarta de pollo a la mexicana";
$receta19->categoria = "Entrantes";
$receta19->pais = "Mexico";
$receta19->descripcion = "La receta que te propongo es la de una tarta de pollo a la mexicana que se presenta como una tarta salada tradicional pero con un sabor muy explosivo como lo puede llegar a ser la gastronomía mexicana. Es una receta bastante sencilla de elaborar por lo que no necesitaras ni de mucho tiempo ni de conocimientos de cocina particulares.";
$receta19->ingredientes = ['Pechuga de pollo', 'Tomate', 'Cebolla','Salsa verde mexicana', 'Hojaldre lámina', 'Queso Mozzarella', 'Sal', 'Pimienta negra molida'];
$receta19->ingredientes2 = ['2', '1', '1', '120g', '250g', '120g', 'Al gusto', 'Al gusto'];
$receta19->elaboracion = "Cocemos las pechugas de pollo en agua con sal durante 20 minutos. Las escurrimos y deshebramos. Cortamos la cebolla en juliana que ponemos a freír en un sartén con un poco de aceite. Picamos el tomate y lo agregamos a la cebolla junto con el pollo. Salpimentamos y revolvemos. Dejamos cinco minutos, a fuego medio, antes de verter la salsa verde. Dejamos otros 12 a 12 minutos en la lumbre medio alta hasta para que se evapore parte de la salsa. Colocamos la masa de hojaldre en un molde circular previamente recubierto de papel antiadherente. Vaciamos todo el contenido del sartén en la tarta. Cortamos el queso mozzarella que esparcimos. Horneamos a 180ºC unos 25 minutos aproximadamente. Cuando esté listo dejamos reposar unos minutos fuera del horno antes de servir.";
$receta19->dificultad = "Media";
$receta19->addFoto("http://www.delicias.tv/blog/wp-content/uploads/2017/03/tarta-768x398.jpg");
$receta19->addFoto("https://2.bp.blogspot.com/-QribVEg7OtQ/VWPAJzVjQNI/AAAAAAAAHGk/weWR8EvDCYE/s1600/tarta-pollo.jpg");
$receta19->addFoto("https://nadirtips.com/wp-content/uploads/2017/02/TartaDePollo-FUENTE-Miercolesdeplaza.com_-678x470.jpg");
$receta19->addFoto("https://assets.unileversolutions.com/recipes-v2/38503.jpg");


$receta20 = new Receta();
$receta20->nombre = "Filetes de pescado al tequila en salsa cítrica";
$receta20->categoria = "Pescados";
$receta20->pais = "Mexico";
$receta20->descripcion = "Se trata de unos deliciosos filetes de pescado al tequila, que puedes replicar el día que quieras (pero para qué esperas, si lo puedes hacer hoy) y sorprender hasta al más incrédulo de tu familia. Los ingredientes son súper comunes, así que no dudamos que logres el platillo en menos de media hora.";
$receta20->ingredientes = ['Filete de bacalao', 'Mantequilla', 'Cebolla blanca picada','Cebollines de ajo picados', 'Dientes de ajo picados', 'Tequila', 'Jugo de limón', 'Hojuelas chile seco rojo', 'Cilantro', 'Pasta larga', 'Ralladura de limón', 'Jitomates cherry'];
$receta20->ingredientes2 = ['500g', '120g', '200g', '3', '3', '120ml', '300ml', '12g', '12g', '300g', '12g', '3'];
$receta20->elaboracion = "Como os digo, en esto de los gazpachos la cosa va en gustos. Aunque en mi familia, jamás se perdona añadir cebolla o pimiento rojo a un gazpacho, la cebolla a veces se puede consentir muy picadita como guarnición -no dentro de los ingredientes triturados-, pero este punto será seguramente controvertido porque en otras casas se hará con cebolla siempre. Lo dejo a vuestro criterio. Lo del pimiento rojo es más delicado, puesto que siempre que se hacen gazpachos andaluces se debe hacer con pimiento verde que aporta un sabor peculiar que no encaja con el dulce del pimiento rojo carnoso. Digo lo mismo. Habrá casas donde se suela hacer con pimiento rojo, pues de acuerdo, pero si queréis hacerme caso, probad con mi receta y dejad el pimiento rojo para otros platos deliciosos como la ensalada de pimientos. Troceamos todos los ingredientes indicados en la proporción que os he puesto y añadimos 50 ml de aceite de oliva, 250 ml de agua de la nevera y 50 ml de vinagre de Jerez, triturando todo en la batidora de vaso o Turmix. No es necesario pelar los tomates o los pimientos porque luego lo vamos a pasar por el colador fino. Si tenéis un robot tipo Thermomix podéis poner todos los ingredientes en el vaso y triturarlos a máxima velocidad durante 4 minutos para obtener una textura perfecta. Una vez triturado, pasamos el gazpacho resultante por el colador fino, apretando con un cucharón para que quede una crema sin pieles ni semillas y lo metemos en la nevera un par de horas para que enfríe bien.";
$receta20->dificultad = "Fácil";
$receta20->addFoto("https://i.blogs.es/bcca3a/fettuccini/650_1200.jpg");
$receta20->addFoto("https://i.ytimg.com/vi/YIlHMLTTrdg/maxresdefault.jpg");
$receta20->addFoto("https://images-gmi-pmc.edge-generalmills.com/a432e478-3ee7-47d3-a9e5-013698ece315.jpg");
$receta20->addFoto("https://d1nsq2txwd94q7.cloudfront.net/public/files/production/recipes/images/3680/fancy/r_3680_1550830836.jpg");



$misRecetas = new Recetas();
$misRecetas->addReceta($receta1);
$misRecetas->addReceta($receta2);
$misRecetas->addReceta($receta3);
$misRecetas->addReceta($receta4);
$misRecetas->addReceta($receta5);
$misRecetas->addReceta($receta6);
$misRecetas->addReceta($receta7);
$misRecetas->addReceta($receta8);
$misRecetas->addReceta($receta9);
$misRecetas->addReceta($receta10);
$misRecetas->addReceta($receta11);
$misRecetas->addReceta($receta12);
$misRecetas->addReceta($receta13);
$misRecetas->addReceta($receta14);
$misRecetas->addReceta($receta15);
$misRecetas->addReceta($receta16);
$misRecetas->addReceta($receta17);
$misRecetas->addReceta($receta18);
$misRecetas->addReceta($receta19);
$misRecetas->addReceta($receta20);


?>