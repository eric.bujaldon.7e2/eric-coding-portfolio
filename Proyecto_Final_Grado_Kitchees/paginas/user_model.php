<?php

class User{
    public $ID;
    public $username;
    public $name;
    public $email;
    public $pass;
    public $pfp;
    public $subscription;

    function getID(){
        return $this->ID;
    }

    function toArray(){
        $tmpArray = array(
            "username"=>$this->username,
            "name"=>$this->name,
            "email"=>$this->email,
            "pass"=>$this->pass,
            "pfp"=>$this->pfp,
            "subscription"=>$this->subscription
          );
          return $tmpArray;
      }
}

class UserList{
    public $users = [];

    function addUser($us){
        $this->users[] = $us;
    }

    function getUserByID($ID){
        foreach($this->users as $us){
            if ($us->ID == $ID){
                return $us;
            }
        }
        return "Este usuario no existe";
    }

    function llistar(){
        foreach($this->users as $us){
            print_r($us);
        }
    }
}
?>