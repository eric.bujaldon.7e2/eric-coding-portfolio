
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitchees</title>


    <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">

    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="../assets/css/style.css" rel="stylesheet">


    <?php
    $page = 1;
    session_start();

    include("DB_connexio.php");
    ?>

    <script>
        function cre(){
            document.getElementById("recetas").innerHTML = "<?php
            $currentUser = $_SESSION['session_nombreUsuario'];
            $currentID = $_GET['idUser'];
            $result2 = $conn->query("SELECT * FROM usuarios WHERE username = '$currentUser' AND ID = '$currentID' ");
                if ($result2 -> num_rows >0){
                    echo '<section class=\'about\'>';
                        echo '<div class=\'container\'>';
                            echo '<div class=\'row\'>';
                            $result = $conn->query("SELECT * FROM favorites INNER JOIN usuarios ON favorites.userID = usuarios.ID
                            INNER JOIN recetas ON favorites.productID = recetas.ID WHERE usuarios.ID = $currentID");
                                if($result->num_rows > 0){
                                    while($row = $result->fetch_assoc()){
                                    echo '<div class=\'boxe col-md-6 \' data-aos=\'zoom-in\'><a href=\'receta.php?id=' . $row['ID'] . '\'>';
                                        echo '<div class=\'pb-3\' ></div>';
                                        echo '<img style=\'width:100px; float: left; margin-right: 10px\' src=\'uploads/recetas/' . $row['foto1'] . '\'/>';
                                        echo '<h4>'. $row['nombre'] .'</h4>';
                                        echo '<p>'. $row['descripcion'] .'</p>';
                                        echo '<p>'. $row['categoria'] .'  |  '. $row['dificultad'] .' </p>';
                                      echo '</a></div>';
                                    }
                                }else {
                                    echo '<div class=\' d-flex justify-content-center col-md-12 \' data-aos=\'zoom-in\'>';                                                                     
                                        echo '<h4>Aún no has añadido ninguna receta a tu lista de favoritas, empieza ya!</h4><br>';
                                      echo '</div>';
                                    echo '<div class=\' d-flex justify-content-center col-md-12 \' data-aos=\'zoom-in\'>';                                                                      
                                      echo '<button type=\'button\' class=\'btn btn-secondary \'><a id =\'cierreSE\'  href=\'paises.php\'>Buscar recetas</a></button>';                                                                   
                                    echo '</div>';
                                    
                                }   
                            echo '</div>';
                        echo'</div>';
                    echo '</section>';
                }else{
                    header('location: miPerfil.php');
                }    
        ?>";
        }
        function buscaRecetas(p, i){
            document.getElementById("recetas").innerHTML = "";
            console.log(p)
            console.log(i)
            if (p.length==0) {
                document.getElementById("recetas").innerHTML = "";
                return;
            }
            var xmlhttp;
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
                //alert("El navegador si soporta XMLHTTP");
            } else {
                //IE 5y6
                xmlhttp = new ActiveXObject("Microsoft.HMLHTTP");
                alert("Tu navegador no soporta XMLHTTP");
            }
            xmlhttp.onreadystatechange = function(){
                //console.log(xmlhttp.readyState,xmlhttp.status);
                if(xmlhttp.readyState==4){
                    if(xmlhttp.status==200){
                        //Buidem la informació a la sortida
                        document.getElementById("recetas").innerHTML=xmlhttp.responseText;
                    } else {
                        document.getElementById("recetas").innerHTML="<p>Error en el proceso de lectura del archivo. Error "+xmlhttp.status;
                    }
                }
            }
            //GET o POST, url, true=>Asincrono false=>Sincrono
            xmlhttp.open("GET","recetillas2.php?q="+p+"&id="+i,true);
            //Ejecutamos la lectura
            xmlhttp.send();
        }
        window.onload =cre;
    </script>
        

</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="../index.html"><img src="../assets/img/kitcheeslogo.png" width="300" height="80"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="../index.html#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#about">Sobre nosotros</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#services">Servicios</a></li>
                    <li><a class="nav-link scrollto" href="../index.html#contact">Contact</a></li>
                    <li><a class="nav-link scrollto" href="../paginas/paises.php">Recetas</a></li>
                    <?php include('perfil.php');?>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

        </div>
    </header>
    
    <div class="section-title">
        <p style="margin-top: 100px">Tus recetas favoritas</p>
        <?php
            $user = $_SESSION['session_nombreUsuario'];

            $query = $conn->query("SELECT * FROM usuarios WHERE username = '$user'");
            foreach($query as $row) {
                $imageURL = 'uploads/usuarios/'.$row["pfp"];
                
            }
            echo"<img id='login' class='log1' src='$imageURL'><br>";
    
        ?>
    </div>
    

    <div class="text-center" >
        <?php
        $cat = [];
        $result1 = $conn->query("SELECT * FROM favorites INNER JOIN usuarios ON favorites.userID = usuarios.ID
        INNER JOIN recetas ON favorites.productID = recetas.ID WHERE usuarios.ID = $currentID");
        foreach($result1 as $row){
            $cat[] = $row['categoria'];
        }
        $cat2 = array_unique($cat);
        $cat3 = [];
        foreach($cat2 as $cat){
            $cat3[] = $cat;
        }

        $cat4 = [];
        foreach($result1 as $row){
            $cat4[] = $row['categoria'];
            
        }
        $cat5 = array_unique($cat4);

        echo'<div class=\'text-center btn-group\' role=\'group\' aria-label=\'Basic example\'>';
        foreach($cat5 as $cat){
            echo '<button onclick="buscaRecetas(\''.$cat.'\', \''.$currentID.'\')" type=\'button\' class=\'btn btn-secondary\'>'.$cat.'</button>';  
        }
        echo '</div>'
        ?>
    </div>
    
    <p></br><span id="recetas">  
    </span></p>

    

    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 footer-contact">
                        <img src="../assets/img/kitcheeslogo.png" width="220" height="70">
                        <p>
                            Mall de Dubai <br>
                            Financial Center Street<br>
                            Dubai - Emirates Árabes <br><br>
                            <strong>Teléfono:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> kitcheesBCN@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Links útiles</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#hero">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#about">Sobre nosotros</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Servicios</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#faq">Preguntas frecuentes</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#contact">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestros Servicios</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Acceder a recetas de todo el
                                    mundo</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poder crear tus propias
                                    recetas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Poseer una lista de recetas
                                    favoritas</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="../index.html#services">Generación de receta aleatoria y
                                    mucho más</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Nuestras Redes Sociales</h4>
                        <p>Síguenos en todas nuestras redes! <br>Te esperamos!</p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <script src="../assets/vendor/aos/aos.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="../assets/vendor/php-email-form/validate.js"></script>

    <script src="../assets/js/main.js"></script>
</body>

</html>
