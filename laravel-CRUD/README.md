# This is a Laravel CRUD Web Application made by Eric
# How to run the app

### Starting Laravel development server command:

php artisan serve

### Write the url given to you in the browser and "/peoples" next to it, example:

http://127.0.0.1:8000/peoples

## Here we can see the main page of the Laravel app:
![Image of the app](laravel-crud.PNG "Laravel CRUD")

## Edit page
![Image of the app](edit-crud.PNG "Laravel CRUD")

## Show more details page
![Image of the app](show-crud.PNG "Laravel CRUD")
