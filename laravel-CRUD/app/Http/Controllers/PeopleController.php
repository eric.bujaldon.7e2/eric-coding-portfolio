<?php
  
namespace App\Http\Controllers;
   
use App\Models\People;
use Illuminate\Http\Request;
  
class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peoples = People::latest()->paginate(10);
    
        return view('peoples.index',compact('peoples'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peoples.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'profession' => 'required',
            'birthday' => 'required',
            'comment' => 'required'
        ]);
    
        People::create($request->all());
     
        return redirect()->route('peoples.index')
                        ->with('success','Persona creada correctament!');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show(People $people)
    {
        return view('peoples.show',compact('people'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function edit(People $people)
    {
        return view('peoples.edit',compact('people'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, People $people)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'profession' => 'required',
            'birthday' => 'required',
            'comment' => 'required'
        ]);
    
        $people->update($request->all());
    
        return redirect()->route('peoples.index')
                        ->with('success','Persona editada correctament!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy(People $people)
    {
        $people->delete();
    
        return redirect()->route('peoples.index')
                        ->with('success','Persona eliminada correctament!');
    }
}