@extends('peoples.layout')
  
@section('content')



<section class="p-0">
    <div class="container">
      <div class="row text-center">

        <div class="col-lg-12 my-5">
                <h2>Mostrar persona</h2>
    </div>
   
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom:</strong>
                {{ $people->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Edat:</strong>
                {{ $people->age }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ocupació:</strong>
                {{ $people->profession }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Aniversari:</strong>
                {{ $people->birthday }} 
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comentari adicional:</strong>
                {{ $people->comment }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 mt-4">
        <a class="btn btn-primary" href="{{ route('peoples.index') }}">Tornar</a>
        </div>

</div>
</div>
</section>
@endsection