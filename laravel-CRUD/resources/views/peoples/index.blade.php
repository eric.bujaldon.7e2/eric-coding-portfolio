@extends('peoples.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Agenda de persones conegudes</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('peoples.create') }}">Afegir nova persona</a>
            </div>
        </div>
    </div>
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Edat</th>
            <th>Ocupació</th>
            <th>Aniversari</th>
            <th>Comentari adicional</th>

            <th width="260px">Acció</th>
        </tr>
        @foreach ($peoples as $people)
        <tr>
            <td><strong>{{ ++$i }}</strong></td>
            <td>{{ $people->name }}</td>
            <td>{{ $people->age }}</td>
            <td>{{ $people->profession }}</td>
            <td>{{ $people->birthday }}</td>
            <td>{{ $people->comment }}</td>

            <td>
                <form action="{{ route('peoples.destroy',$people->id) }}" method="POST">
   
                    <a class="btn btn-secondary" href="{{ route('peoples.show',$people->id) }}">Mostrar</a>
                    <br>
                    <a class="btn btn-success" href="{{ route('peoples.edit',$people->id) }}">Editar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

@if ($message = Session::get('success'))
        <div class="alert alert-warning">
            <p>{{ $message }}</p>
        </div>
    @endif
  
    
    {!! $peoples->links() !!}

@endsection