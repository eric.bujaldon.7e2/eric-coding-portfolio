@extends('peoples.layout')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Afegir persones</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-danger" href="{{ route('peoples.index') }}">Cancel·lar</a>
        </div>
    </div>
</div>
  
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Error!</strong>Hi han problemes al teu input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('peoples.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom:</strong>
                <input type="text" name="name" class="form-control" placeholder="Nom">
                @error('name')
                <p class="form-text text-danger">ERROR EN EL NOMBRE</p>
                @enderror
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Edat:</strong>
                <input type="text" name="age" class="form-control" placeholder="Edat">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Aniversari:</strong>
                <input type="text" name="birthday" class="form-control" placeholder="Aniversari">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comentari adicional:</strong>
                <input type="text" name="comment" class="form-control" placeholder="Comentari">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ocupació:</strong>
                <input type="text" name="profession" class="form-control" placeholder="Professió">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-4">
                <button type="submit" class="btn btn-success">Afegir</button>
        </div>
    </div>
   
</form>
@endsection